package ru.microarch.delivery.application.queries.getactiveorders;

import ru.microarch.delivery.application.types.CommandHandler;

import java.util.List;

public abstract class GetActiveOrdersHandler implements CommandHandler<GetActiveOrdersQuery, List<OrderDto>> {

    @Override
    public Class<GetActiveOrdersQuery> getCommandType() {
        return GetActiveOrdersQuery.class;
    }
}
