package ru.microarch.delivery.application.queries.getallcouriers;

import ru.microarch.delivery.application.types.Request;

import java.util.List;

public record GetAllCouriersQuery() implements Request<List<CourierDto>> {
}
