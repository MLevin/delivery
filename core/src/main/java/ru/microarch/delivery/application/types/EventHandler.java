package ru.microarch.delivery.application.types;

import ru.microarch.delivery.util.primitives.DomainEvent;

public interface EventHandler<T extends DomainEvent> {

    void handle(T event);

    Class<T> getEventType();
}
