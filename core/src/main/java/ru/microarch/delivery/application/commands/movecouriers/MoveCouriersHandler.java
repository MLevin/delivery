package ru.microarch.delivery.application.commands.movecouriers;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ru.microarch.delivery.application.types.CommandHandler;
import ru.microarch.delivery.application.types.UnitOfWorkFactory;
import ru.microarch.delivery.domain.courier.aggregate.Courier;
import ru.microarch.delivery.domain.order.aggregate.Order;
import ru.microarch.delivery.ports.CourierRepository;
import ru.microarch.delivery.ports.OrderRepository;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@AllArgsConstructor
@Slf4j
public class MoveCouriersHandler implements CommandHandler<MoveCouriersCommand, Void> {

    private final CourierRepository courierRepository;

    private final OrderRepository orderRepository;

    private final UnitOfWorkFactory unitOfWorkFactory;


    @Override
    public Void handle(MoveCouriersCommand command) {
        List<Order> allAssignedOrders = orderRepository.getAllAssigned();
        if (allAssignedOrders.isEmpty()) {
            return null;
        }

        Map<UUID, Order> courierToOrder = allAssignedOrders.stream()
                .collect(Collectors.toMap(Order::getCourierId, (order -> order)));

        var workingCouriers = courierRepository.getAllBusy();

        for (Courier courier : workingCouriers) {
            var order = courierToOrder.get(courier.getId());
            if (order == null) {
                continue;
            }
            var targetLocation = order.getTargetLocation();
            courier.move(targetLocation);

            var unitOfWork = unitOfWorkFactory.create();

            if (targetLocation.equals(courier.getLocation())) {
                order.complete();
                unitOfWork.submitForUpdate(order);
            }
            unitOfWork.submitForUpdate(courier);
            try {
                unitOfWork.commit();
            } catch (Exception e) {
                log.error("Can't save courier or order after moving the courier", e);
            }
        }
        return null;
    }

    @Override
    public Class<MoveCouriersCommand> getCommandType() {
        return MoveCouriersCommand.class;
    }
}
