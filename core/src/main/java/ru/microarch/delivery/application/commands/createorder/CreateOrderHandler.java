package ru.microarch.delivery.application.commands.createorder;

import lombok.RequiredArgsConstructor;
import ru.microarch.delivery.application.types.CommandHandler;
import ru.microarch.delivery.application.types.UnitOfWorkFactory;
import ru.microarch.delivery.domain.order.aggregate.Order;
import ru.microarch.delivery.domain.sharedkernel.Weight;
import ru.microarch.delivery.ports.GeoClient;

@RequiredArgsConstructor
public class CreateOrderHandler implements CommandHandler<CreateOrderCommand, Void> {
    private final GeoClient geoClient;

    private final UnitOfWorkFactory unitOfWorkFactory;

    @Override
    public Void handle(CreateOrderCommand command) {
        var targetLocation = geoClient.getLocation(command.address());
        var order = new Order(
                command.basketId(),
                targetLocation,
                new Weight(command.weight()));
        var unitOfWork = unitOfWorkFactory.create();
        unitOfWork.submitForCreate(order);
        unitOfWork.commit();
        return null;
    }

    @Override
    public Class<CreateOrderCommand> getCommandType() {
        return CreateOrderCommand.class;
    }
}
