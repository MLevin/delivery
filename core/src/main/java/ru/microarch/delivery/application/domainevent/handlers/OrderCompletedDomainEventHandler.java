package ru.microarch.delivery.application.domainevent.handlers;

import lombok.RequiredArgsConstructor;
import ru.microarch.delivery.application.types.EventHandler;
import ru.microarch.delivery.domain.order.aggregate.events.OrderCompletedDomainEvent;
import ru.microarch.delivery.ports.BusProducer;

@RequiredArgsConstructor
public class OrderCompletedDomainEventHandler implements EventHandler<OrderCompletedDomainEvent> {

    private final BusProducer busProducer;

    @Override
    public void handle(OrderCompletedDomainEvent event) {
        busProducer.publishOrderCompletedDomainEvent(event);
    }

    @Override
    public Class<OrderCompletedDomainEvent> getEventType() {
        return OrderCompletedDomainEvent.class;
    }
}
