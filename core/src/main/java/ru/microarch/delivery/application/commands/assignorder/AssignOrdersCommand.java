package ru.microarch.delivery.application.commands.assignorder;

import ru.microarch.delivery.application.types.Request;


public record AssignOrdersCommand() implements Request<Void> {
}
