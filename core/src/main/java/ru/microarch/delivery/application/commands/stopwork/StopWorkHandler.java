package ru.microarch.delivery.application.commands.stopwork;

import lombok.RequiredArgsConstructor;
import ru.microarch.delivery.application.types.CommandHandler;
import ru.microarch.delivery.application.types.UnitOfWorkFactory;
import ru.microarch.delivery.domain.courier.aggregate.Courier;
import ru.microarch.delivery.ports.CourierRepository;

@RequiredArgsConstructor
public class StopWorkHandler implements CommandHandler<StopWorkCommand, Void> {

    private final CourierRepository courierRepository;

    private final UnitOfWorkFactory unitOfWorkFactory;

    @Override
    public Void handle(StopWorkCommand command) {
        Courier courier = courierRepository.get(command.courierId());
        courier.stopWork();
        var unitOfWork = unitOfWorkFactory.create();
        unitOfWork.submitForUpdate(courier);
        unitOfWork.commit();
        return null;
    }

    @Override
    public Class<StopWorkCommand> getCommandType() {
        return StopWorkCommand.class;
    }
}
