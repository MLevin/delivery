package ru.microarch.delivery.application.domainevent.handlers;

import lombok.RequiredArgsConstructor;
import ru.microarch.delivery.application.types.EventHandler;
import ru.microarch.delivery.domain.order.aggregate.events.OrderCreatedDomainEvent;
import ru.microarch.delivery.ports.BusProducer;

@RequiredArgsConstructor
public class OrderCreatedDomainEventHandler implements EventHandler<OrderCreatedDomainEvent> {
    private final BusProducer busProducer;

    @Override
    public void handle(OrderCreatedDomainEvent event) {
        busProducer.publishOrderCreatedDomainEvent(event);
    }

    @Override
    public Class<OrderCreatedDomainEvent> getEventType() {
        return OrderCreatedDomainEvent.class;
    }
}
