package ru.microarch.delivery.application.commands.createorder;


import ru.microarch.delivery.application.types.Request;

import java.util.Objects;
import java.util.UUID;

public record CreateOrderCommand(UUID basketId, String address, int weight) implements Request<Void> {

    public CreateOrderCommand {
        Objects.requireNonNull(basketId, "Basket ID cannot be null.");
        Objects.requireNonNull(address, "Address cannot be null.");

        if (address.trim().isEmpty()) {
            throw new IllegalArgumentException("Address cannot be empty.");
        }

        if (weight <= 0) {
            throw new IllegalArgumentException("Weight must be greater than 0.");
        }
    }
}
