package ru.microarch.delivery.application.queries.getactiveorders;

import ru.microarch.delivery.application.queries.shareddto.LocationDto;

import java.util.UUID;

public record OrderDto(UUID id, LocationDto location) {
}
