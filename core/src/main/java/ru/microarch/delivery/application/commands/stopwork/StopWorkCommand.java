package ru.microarch.delivery.application.commands.stopwork;

import ru.microarch.delivery.application.types.Request;

import java.util.Objects;
import java.util.UUID;

public record StopWorkCommand(UUID courierId) implements Request<Void> {

    public StopWorkCommand {
        Objects.requireNonNull(courierId, "Courier ID cannot be null.");
    }
}
