package ru.microarch.delivery.application.commands.startwork;

import lombok.RequiredArgsConstructor;
import ru.microarch.delivery.application.types.CommandHandler;
import ru.microarch.delivery.application.types.UnitOfWorkFactory;
import ru.microarch.delivery.domain.courier.aggregate.Courier;
import ru.microarch.delivery.ports.CourierRepository;

@RequiredArgsConstructor
public class StartWorkHandler implements CommandHandler<StartWorkCommand, Void> {

    private final CourierRepository courierRepository;

    private final UnitOfWorkFactory unitOfWorkFactory;

    @Override
    public Void handle(StartWorkCommand command) {
        Courier courier = courierRepository.get(command.courierId());
        courier.startWork();
        var unitOfWork = unitOfWorkFactory.create();
        unitOfWork.submitForUpdate(courier);
        unitOfWork.commit();
        return null;
    }

    @Override
    public Class<StartWorkCommand> getCommandType() {
        return StartWorkCommand.class;
    }
}
