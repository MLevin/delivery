package ru.microarch.delivery.application.types;

import ru.microarch.delivery.util.primitives.Aggregate;

public interface UnitOfWork {

    void submit(Runnable runnable);

    <T extends Aggregate> void submitForCreate(T aggregate);

    <T extends Aggregate>  void submitForUpdate(T aggregate);

    void commit();
}
