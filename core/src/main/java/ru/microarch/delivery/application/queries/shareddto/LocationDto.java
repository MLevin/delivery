package ru.microarch.delivery.application.queries.shareddto;

public record LocationDto(int x, int y) {
}
