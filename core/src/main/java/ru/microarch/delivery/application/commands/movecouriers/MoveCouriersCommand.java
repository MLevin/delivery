package ru.microarch.delivery.application.commands.movecouriers;

import ru.microarch.delivery.application.types.Request;


public record MoveCouriersCommand() implements Request<Void> {
}
