package ru.microarch.delivery.application.domainevent.handlers;

import lombok.RequiredArgsConstructor;
import ru.microarch.delivery.application.types.EventHandler;
import ru.microarch.delivery.domain.order.aggregate.events.OrderAssignedDomainEvent;
import ru.microarch.delivery.ports.BusProducer;

@RequiredArgsConstructor
public class OrderAssignedDomainEventHandler implements EventHandler<OrderAssignedDomainEvent> {

    private final BusProducer busProducer;

    @Override
    public void handle(OrderAssignedDomainEvent event) {
        busProducer.publishOrderAssignedDomainEvent(event);
    }

    @Override
    public Class<OrderAssignedDomainEvent> getEventType() {
        return OrderAssignedDomainEvent.class;
    }
}
