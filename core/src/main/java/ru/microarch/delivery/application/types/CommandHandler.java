package ru.microarch.delivery.application.types;

public interface CommandHandler<C extends Request<T>, T> {
    T handle(C command);
    Class<C> getCommandType();
}
