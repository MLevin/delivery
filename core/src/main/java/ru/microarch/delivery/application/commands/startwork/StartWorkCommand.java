package ru.microarch.delivery.application.commands.startwork;

import ru.microarch.delivery.application.types.Request;

import java.util.Objects;
import java.util.UUID;

public record StartWorkCommand(UUID courierId) implements Request<Void> {

    public StartWorkCommand {
        Objects.requireNonNull(courierId, "Courier ID cannot be null.");
    }
}
