package ru.microarch.delivery.application.commands.assignorder;

import lombok.RequiredArgsConstructor;
import ru.microarch.delivery.application.types.CommandHandler;
import ru.microarch.delivery.application.types.UnitOfWorkFactory;
import ru.microarch.delivery.domain.courier.aggregate.Courier;
import ru.microarch.delivery.domain.order.aggregate.Order;
import ru.microarch.delivery.domainservices.DispatchService;
import ru.microarch.delivery.ports.CourierRepository;
import ru.microarch.delivery.ports.OrderRepository;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class AssignOrdersHandler implements CommandHandler<AssignOrdersCommand, Void> {
    private final DispatchService dispatchService;

    private final OrderRepository orderRepository;

    private final CourierRepository courierRepository;

    private final UnitOfWorkFactory unitOfWorkFactory;

    @Override
    public Void handle(AssignOrdersCommand command) {
        List<Order> notAssignedOrders = orderRepository.getAllCreated();
        if (notAssignedOrders.isEmpty()) {
            return null;
        }
        var firstOrder = notAssignedOrders.get(0); // TODO: cant use getFirst() for some reason
        var readyCouriers = courierRepository.getAllReady();
        Optional<Courier> assignedCourier = dispatchService.dispatch(firstOrder, readyCouriers);
        if (assignedCourier.isEmpty()) {
            return null;
        }
        assignOrder(firstOrder, assignedCourier.get());
        return null;
    }

    private void assignOrder(Order order, Courier courier) {
        order.assign(courier);
        var unitOfWork = unitOfWorkFactory.create();
        unitOfWork.submitForUpdate(order);
        unitOfWork.submitForUpdate(courier);
        unitOfWork.commit();
    }

    @Override
    public Class<AssignOrdersCommand> getCommandType() {
        return AssignOrdersCommand.class;
    }
}
