package ru.microarch.delivery.application.queries.getallcouriers;

import ru.microarch.delivery.application.types.CommandHandler;

import java.util.List;

public abstract class GetAllCouriersHandler implements CommandHandler<GetAllCouriersQuery, List<CourierDto>> {

    @Override
    public Class<GetAllCouriersQuery> getCommandType() {
        return GetAllCouriersQuery.class;
    }
}
