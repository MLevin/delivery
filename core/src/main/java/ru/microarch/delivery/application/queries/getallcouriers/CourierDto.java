package ru.microarch.delivery.application.queries.getallcouriers;

import ru.microarch.delivery.application.queries.shareddto.LocationDto;

import java.util.UUID;


public record CourierDto(UUID id, String name, LocationDto location) {
}
