package ru.microarch.delivery.application.types;

public interface UnitOfWorkFactory {

    UnitOfWork create();
}
