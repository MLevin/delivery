package ru.microarch.delivery.application.queries.getactiveorders;

import ru.microarch.delivery.application.types.Request;

import java.util.List;

public record GetActiveOrdersQuery() implements Request<List<OrderDto>> {
}
