package ru.microarch.delivery.util.primitives;

import java.util.ArrayList;
import java.util.List;

public abstract class Aggregate {

    private final List<DomainEvent> domainEvents = new ArrayList<>();

    public void raiseDomainEvent(DomainEvent domainEvent) {
        domainEvents.add(domainEvent);
    }

    public List<DomainEvent> getDomainEvents() {
        return List.copyOf(domainEvents);
    }
}
