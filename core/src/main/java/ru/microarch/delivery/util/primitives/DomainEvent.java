package ru.microarch.delivery.util.primitives;

import lombok.Getter;

import java.util.UUID;

@Getter
public abstract class DomainEvent {

    private final UUID eventId = UUID.randomUUID();

}
