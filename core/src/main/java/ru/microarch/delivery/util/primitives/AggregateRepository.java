package ru.microarch.delivery.util.primitives;

import java.util.UUID;

public interface AggregateRepository<C extends Aggregate> {

    C add(C order);

    void update(C order);

    C get(UUID id);

    Class<C> getAggregateType();
}
