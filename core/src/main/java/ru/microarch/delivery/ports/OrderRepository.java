package ru.microarch.delivery.ports;


import ru.microarch.delivery.domain.order.aggregate.Order;
import ru.microarch.delivery.util.primitives.AggregateRepository;

import java.util.List;

public interface OrderRepository extends AggregateRepository<Order> {
    List<Order> getAllCreated();

    List<Order> getAllAssigned();
}
