package ru.microarch.delivery.ports;

import ru.microarch.delivery.domain.sharedkernel.Location;

public interface GeoClient {
    Location getLocation(String address);
}
