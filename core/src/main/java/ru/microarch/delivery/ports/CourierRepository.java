package ru.microarch.delivery.ports;

import ru.microarch.delivery.domain.courier.aggregate.Courier;
import ru.microarch.delivery.util.primitives.AggregateRepository;

import java.util.List;

public interface CourierRepository extends AggregateRepository<Courier> {

    List<Courier> getAllReady();

    List<Courier> getAllBusy();
}
