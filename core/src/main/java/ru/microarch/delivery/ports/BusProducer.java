package ru.microarch.delivery.ports;

import ru.microarch.delivery.domain.order.aggregate.events.OrderAssignedDomainEvent;
import ru.microarch.delivery.domain.order.aggregate.events.OrderCompletedDomainEvent;
import ru.microarch.delivery.domain.order.aggregate.events.OrderCreatedDomainEvent;

public interface BusProducer {

    void publishOrderCreatedDomainEvent(OrderCreatedDomainEvent event);

    void publishOrderAssignedDomainEvent(OrderAssignedDomainEvent event);

    void publishOrderCompletedDomainEvent(OrderCompletedDomainEvent event);
}
