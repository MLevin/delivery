package ru.microarch.delivery.domainservices;

import ru.microarch.delivery.domain.courier.aggregate.Courier;
import ru.microarch.delivery.domain.courier.aggregate.CourierStatus;
import ru.microarch.delivery.domain.order.aggregate.Order;
import ru.microarch.delivery.domain.order.aggregate.OrderStatus;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class DispatchService {

    public Optional<Courier> dispatch(Order order, List<Courier> couriers) {
        if (order.getStatus() != OrderStatus.CREATED) {
            throw new IllegalStateException("Order is already being handled. Order id=" + order.getId());
        }
        return couriers.stream()
                .filter(courier -> courier.getStatus() == CourierStatus.READY)
                .filter(courier -> courier.isCapableOfCaring(order.getWeight()))
                .min(Comparator.comparingInt(courier -> courier.calculateTimeToPoint(order.getTargetLocation())));
    }
}
