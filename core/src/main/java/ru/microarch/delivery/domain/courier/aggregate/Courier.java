package ru.microarch.delivery.domain.courier.aggregate;


import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import ru.microarch.delivery.domain.sharedkernel.Location;
import ru.microarch.delivery.domain.sharedkernel.Weight;
import ru.microarch.delivery.util.primitives.Aggregate;

import java.util.UUID;

/**
 * Courier - это курьер, он состоит из:
 * <ul>
 *   <li>Id - идентификатор курьера.</li>
 *   <li>Name - имя курьера.</li>
 *   <li>Transport - транспорт курьера.</li>
 *   <li>Location - местоположение курьера, начальная локация при создании (1,1).</li>
 *   <li>Status - статус курьера, начальный статус при создании NotAvailable (недоступен).</li>
 * </ul>
 *
 * <p>Курьер может быть создан. При создании нужно передать Name, Transport.</p>
 *
 * <p>Курьер может начать рабочий день. При этом статус меняется на Ready (готов к работе).
 * Если он занят (выполняет заказ), то он не может начать рабочий день.</p>
 *
 * <p>Курьер может закончить рабочий день. При этом статус меняется на NotAvailable (недоступен).
 * Если он занят (выполняет заказ), то он не может закончить рабочий день.</p>
 *
 * <p>Заказ может быть назначен на курьера. При этом статус курьера меняется на Busy (занят).
 * Если курьер уже Busy (занят), то повторное назначение является ошибкой.
 * Если курьер NotAvailable (недоступен), то назначение заказа также является ошибкой.</p>
 *
 * <p>Курьер может переместиться на один шаг в сторону Location заказа. Размер шага равен скорости его транспорта.
 * Курьер может ходить как горизонтально, так и вертикально, но не наискосок.</p>
 *
 * <p>Если транспорт курьера движется, например, со скоростью 4 клетки за шаг, а заказ находится ближе,
 * например в 2 клетках, то курьер должен переместиться только до Location заказа.</p>
 *
 * <p>Если курьер достиг Location заказа, то:
 * <ul>
 *   <li>Заказ завершается (переходит в статус Completed).</li>
 *   <li>Курьер становится свободным (переходит в статус Ready).</li>
 * </ul>
 * </p>
 *
 * <p>Курьер должен уметь возвращать количество шагов, которое он потенциально затратит на путь до локации заказа.
 * При расчете нужно учесть скорость транспорта курьера. Например, если есть курьер на велосипеде,
 * находящийся в точке (1,1), а заказ в точке (5,5), курьеру надо пройти 4 клетки по горизонтали и 4 по вертикали.
 * Суммарная дистанция равна 8 клеткам, и при скорости велосипеда в 2 клетки за шаг, курьеру нужно 4 шага,
 * чтобы доставить заказ.</p>
 */

@Getter
@Builder(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class Courier extends Aggregate {
    public static final Location DEFAULT_LOCATION = new Location(1, 1);

    @EqualsAndHashCode.Include
    private UUID id;

    private String name;

    private Transport transport;

    private Location location;

    private CourierStatus status;

    private int version;


    public Courier(String name, Transport transport) {
        this(UUID.randomUUID(), name, transport, DEFAULT_LOCATION, CourierStatus.NOT_AVAILABLE, 0);
    }

    public void startWork() {
        if (status == CourierStatus.READY) {
            throw new IllegalStateException("The courier is already working");
        }
        status = CourierStatus.READY;
    }

    public void assignOrder() {
        if (status != CourierStatus.READY) {
            throw new IllegalStateException("The courier is not ready and cannot take an order");
        }
        status = CourierStatus.BUSY;
    }

    public void stopWork() {
        if (status == CourierStatus.BUSY) {
            throw new IllegalStateException("The courier is busy and cannot stop work");
        }
        status = CourierStatus.NOT_AVAILABLE;
    }

    public int calculateTimeToPoint(Location targetLocation) {
        int distance = location.distanceTo(targetLocation);
        return distance / transport.getSpeed();
    }

    public void move(Location targetLocation) {
        if (status != CourierStatus.BUSY) {
            throw new IllegalStateException("The courier can't move before an order is assigned");
        }
        int movePoints = transport.getSpeed();
        location = location.moveTo(targetLocation, movePoints);

        if (location.equals(targetLocation)) {
            status = CourierStatus.READY;
        }
    }

    public boolean isCapableOfCaring(Weight weight) {
        return transport.isCapableOfCaring(weight);
    }
}
