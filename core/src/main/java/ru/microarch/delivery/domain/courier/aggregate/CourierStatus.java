package ru.microarch.delivery.domain.courier.aggregate;

import lombok.Getter;

@Getter
public enum CourierStatus {
    NOT_AVAILABLE(1, "NotAvailable"),
    READY(2, "Ready"),
    BUSY(3, "Busy");

    private final int id;

    private final String name;

    CourierStatus(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public static CourierStatus fromString(String name) {
        if (name != null) {
            for (CourierStatus status : CourierStatus.values()) {
                if (name.equalsIgnoreCase(status.name)) {
                    return status;
                }
            }
        }
        throw new IllegalArgumentException("No constant with a name " + name + " found in Enum CourierStatus");
    }

    public static CourierStatus fromId(int id) {
        for (CourierStatus status : CourierStatus.values()) {
            if (id == status.id) {
                return status;
            }
        }
        throw new IllegalArgumentException("No constant with id=" + id + " found in Enum CourierStatus");
    }
}