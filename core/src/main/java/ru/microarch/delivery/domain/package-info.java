/**
 * {@link ru.microarch.delivery.domain core.domain} директория содержит классы относящиеся к Domain layer
 * такие как Aggregate, Entity, and ValueObject.
 */

package ru.microarch.delivery.domain;