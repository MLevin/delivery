package ru.microarch.delivery.domain.courier.aggregate;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import ru.microarch.delivery.domain.sharedkernel.Weight;

import java.util.HashMap;
import java.util.Map;

/**
 * Transport - это транспорт курьера, он состоит из:
 * <ul>
 *   <li>Id - идентификатор транспортного средства.</li>
 *   <li>Name - название транспорта.</li>
 *   <li>Speed - скорость транспорта.</li>
 *   <li>Capacity - грузоподъемность транспорта.</li>
 * </ul>
 *
 * <p>Transport бывает 4 видов:</p>
 * <ul>
 *   <li>Pedestrian (пешеход) - Speed = 1, Capacity = 1.</li>
 *   <li>Bicycle (велосипедист) - Speed = 2, Capacity = 4.</li>
 *   <li>Scooter (мопед) - Speed = 3, Capacity = 6.</li>
 *   <li>Car (автомобиль) - Speed = 4, Capacity = 8.</li>
 * </ul>
 *
 * <p>Сущность должна иметь метод, который отвечает на вопрос "может ли данный транспорт перевезти определенный вес?"</p>
 *
 * <p>Этот метод должен сравнивать переданный вес с Capacity транспортного средства и возвращать true, если транспорт способен его перевезти, иначе false.</p>
 */

@Getter
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public final class Transport {

    public static final Transport PEDESTRIAN = new Transport(1, "Pedestrian", 1, new Weight(1));
    public static final Transport BICYCLE = new Transport(2, "Bicycle", 2, new Weight(4));
    public static final Transport SCOOTER = new Transport(3, "Scooter", 3, new Weight(6));
    public static final Transport CAR = new Transport(4, "Car", 4, new Weight(8));

    private static final Map<String, Transport> NAME_TO_TRANSPORT_MAP = new HashMap<>();
    private static final Map<Integer, Transport> UUID_TO_TRANSPORT_MAP = new HashMap<>();

    private static void putToNameMaps(Transport transport) {
        NAME_TO_TRANSPORT_MAP.put(transport.name.toLowerCase(), transport);
        UUID_TO_TRANSPORT_MAP.put(transport.id, transport);
    }

    static {
        putToNameMaps(PEDESTRIAN);
        putToNameMaps(BICYCLE);
        putToNameMaps(SCOOTER);
        putToNameMaps(CAR);
    }


    @EqualsAndHashCode.Include
    private final int id;

    private final String name;

    private final int speed;

    private final Weight capacity;

    public static Transport fromName(String name) {
        var transport = NAME_TO_TRANSPORT_MAP.get(name.toLowerCase());
        if (transport == null) {
            throw new IllegalArgumentException("No transport found with the name: " + name);
        }
        return transport;
    }

    public static Transport fromId(int id) {
        var transport = UUID_TO_TRANSPORT_MAP.get(id);
        if (transport == null) {
            throw new IllegalArgumentException("No transport found with the id: " + id);
        }
        return transport;
    }

    public boolean isCapableOfCaring(Weight weight) {
        return capacity.isGreaterThanOrEqualTo(weight);
    }
}