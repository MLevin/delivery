package ru.microarch.delivery.domain.order.aggregate;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import ru.microarch.delivery.domain.courier.aggregate.Courier;
import ru.microarch.delivery.domain.order.aggregate.events.OrderAssignedDomainEvent;
import ru.microarch.delivery.domain.order.aggregate.events.OrderCompletedDomainEvent;
import ru.microarch.delivery.domain.order.aggregate.events.OrderCreatedDomainEvent;
import ru.microarch.delivery.domain.sharedkernel.Location;
import ru.microarch.delivery.domain.sharedkernel.Weight;
import ru.microarch.delivery.util.primitives.Aggregate;

import java.util.UUID;

/**
 * Order - это заказ, он состоит из:
 * <ul>
 *   <li>Id - идентификатор заказа.</li>
 *   <li>CourierId - идентификатор исполнителя (курьера).</li>
 *   <li>Location - местоположение, куда нужно доставить заказ.</li>
 *   <li>Weight - вес заказа.</li>
 *   <li>Status - статус заказа.</li>
 * </ul>
 *
 * <p>Заказ может быть создан. При создании нужно передать Id, Location, Weight.
 * Id передается на вход, так как основой для заказа будет являться Id корзины.
 * Поэтому мы не генерируем Id, а получаем на вход.</p>
 *
 * <p>При создании заказа проставляется статус Created (создан).</p>
 *
 * <p>Заказ может быть назначен на курьера. При назначении заказ переходит в статус
 * Assigned (назначен на курьера), а в поле CourierId прописывается Id курьера.
 * Если заказ назначен на курьера, то курьер переходит в статус Busy (занят).</p>
 *
 * <p>Заказ может быть завершен. При завершении заказ переходит в статус Completed
 * (завершен), а исполнитель становится свободен (Ready). Завершить можно только
 * назначенный ранее заказ.</p>
 */
@Getter
@Builder(access = AccessLevel.PUBLIC)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public final class Order extends Aggregate {

    @EqualsAndHashCode.Include
    private final UUID id;

    private UUID courierId;

    private final Location targetLocation;

    private final Weight weight;

    private OrderStatus status;

    private int version;


    public Order(UUID id, Location targetLocation, Weight weight) {
        this(id, null, targetLocation, weight, OrderStatus.CREATED, 0 );
        raiseDomainEvent(new OrderCreatedDomainEvent(id));
    }

    public void assign(Courier courier) {
        if (status != OrderStatus.CREATED && status != OrderStatus.ASSIGNED) {
            throw new IllegalStateException(String.format("An order with status=%s cannot be assigned", status));
        }
        courier.assignOrder();
        this.courierId = courier.getId();
        this.status = OrderStatus.ASSIGNED;
        raiseDomainEvent(new OrderAssignedDomainEvent(id));
    }

    public void complete() {
        if (status != OrderStatus.ASSIGNED) {
            throw new IllegalStateException("An order that has not been assigned cannot be completed");
        }
        status = OrderStatus.COMPLETED;
        raiseDomainEvent(new OrderCompletedDomainEvent(id));
    }
}
