package ru.microarch.delivery.domain.order.aggregate;

import lombok.Getter;

@Getter
public enum OrderStatus {
    CREATED("Created"),
    ASSIGNED("Assigned"),
    COMPLETED("Completed");


    private final String name;

    OrderStatus(String name) {
        this.name = name;
    }

    public static OrderStatus fromString(String text) {
        if (text != null) {
            for (OrderStatus status : OrderStatus.values()) {
                if (text.equalsIgnoreCase(status.name)) {
                    return status;
                }
            }
        }
        throw new IllegalArgumentException("No constant with text " + text + " found in Enum OrderStatus");
    }
}