package ru.microarch.delivery.domain.sharedkernel;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Represents a location in a two-dimensional space.
 * <p>
 * This record holds the abscissa (x-coordinate) and the ordinate (y-coordinate) of a point in 2D space.
 * </p>
 *
 * @param x the abscissa value (x-coordinate) of the location, representing the position along the horizontal axis
 * @param y the ordinate value (y-coordinate) of the location, representing the position along the vertical axis
 */
public record Location(int x, int y) {

    public static final int MIN_X = 1;
    public static final int MAX_X = 10;
    public static final int MIN_Y = 1;
    public static final int MAX_Y = 10;


    public Location {
        if (x < MIN_X || x > MAX_X || y < MIN_Y || y > MAX_Y) {
            throw new IllegalArgumentException(
                    String.format("X coordinates must be between %d and %d, and Y coordinates must be between %d and %d",
                            MIN_X, MAX_X, MIN_Y, MAX_Y));
        }
    }

    public static int calculateDistance(Location startLocation, Location targetLocation) {
        return Math.abs(startLocation.x - targetLocation.x) + Math.abs(startLocation.y - targetLocation.y);
    }

    public int distanceTo(Location targetLocation) {
        return calculateDistance(this, targetLocation);
    }

    public static Location randomLocation() {
        int x = MIN_X + ThreadLocalRandom.current().nextInt(MAX_X - MIN_X + 1);
        int y = MIN_Y + ThreadLocalRandom.current().nextInt(MAX_Y - MIN_Y + 1);
        return new Location(x, y);
    }

    public Location moveBy(int dx, int dy) {
        int newX = Math.min(MAX_X, Math.max(MIN_X, this.x + dx));
        int newY = Math.min(MAX_Y, Math.max(MIN_Y, this.y + dy));
        return new Location(newX, newY);
    }

    public Location moveTo(Location targetLocation, int movePoints) {
        int newX = calculateNewCoordinate(this.x(), targetLocation.x(), movePoints);
        movePoints -= Math.abs(newX - this.x()); // Update remaining move points after moving in x direction
        int newY = calculateNewCoordinate(this.y(), targetLocation.y(), movePoints);
        return new Location(newX, newY);
    }

    private int calculateNewCoordinate(int current, int target, int movePoints) {
        int distance = Math.abs(target - current);
        boolean isPositiveDirection = target - current > 0;
        int delta = Math.min(distance, movePoints);
        return current + (isPositiveDirection ? delta : -delta);
    }

}