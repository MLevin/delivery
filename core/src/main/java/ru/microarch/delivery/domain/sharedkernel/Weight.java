package ru.microarch.delivery.domain.sharedkernel;

public record Weight(int kilograms) implements Comparable<Weight> {

    public Weight {
        if (kilograms <= 0) {
            throw new IllegalArgumentException("Weight must be greater than 0");
        }
    }

    @Override
    public int compareTo(Weight other) {
        return Integer.compare(this.kilograms, other.kilograms);
    }

    public boolean isGreaterThan(Weight other) {
        return compareTo(other) > 0;
    }

    public boolean isLessThan(Weight other) {
        return compareTo(other) < 0;
    }

    public boolean isGreaterThanOrEqualTo(Weight other) {
        return compareTo(other) >= 0;
    }

    public boolean isLessThanOrEqualTo(Weight other) {
        return compareTo(other) <= 0;
    }
}