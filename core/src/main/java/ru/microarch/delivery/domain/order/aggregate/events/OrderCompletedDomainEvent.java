package ru.microarch.delivery.domain.order.aggregate.events;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import ru.microarch.delivery.util.primitives.DomainEvent;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class OrderCompletedDomainEvent extends DomainEvent {

    private UUID orderId;

}
