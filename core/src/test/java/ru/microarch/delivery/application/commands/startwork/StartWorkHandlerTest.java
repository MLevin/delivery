package ru.microarch.delivery.application.commands.startwork;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.microarch.delivery.application.types.UnitOfWork;
import ru.microarch.delivery.application.types.UnitOfWorkFactory;
import ru.microarch.delivery.domain.courier.aggregate.Courier;
import ru.microarch.delivery.ports.CourierRepository;

import java.util.UUID;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class StartWorkHandlerTest {

    @Mock
    private CourierRepository courierRepository;

    @Mock
    private UnitOfWork mockUnitOfWork;

    private final UnitOfWorkFactory unitOfWorkFactory = () -> mockUnitOfWork;

    private StartWorkHandler startWorkHandler;

    @BeforeEach
    public void setUp() {
        startWorkHandler = new StartWorkHandler(courierRepository, unitOfWorkFactory);
    }

    @Test
    void testHandleStartWorkHandlerCommand() {
        // given
        StartWorkCommand command = new StartWorkCommand(UUID.randomUUID());
        Courier mockCourier = mock(Courier.class);

        when(courierRepository.get(command.courierId())).thenReturn(mockCourier);

        // when
        startWorkHandler.handle(command);

        // then
        verify(mockCourier).startWork();
        verify(mockUnitOfWork).submitForUpdate(mockCourier);
        verify(mockUnitOfWork).commit();
    }
}