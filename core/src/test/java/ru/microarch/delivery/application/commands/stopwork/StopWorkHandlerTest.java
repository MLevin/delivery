package ru.microarch.delivery.application.commands.stopwork;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.microarch.delivery.application.types.UnitOfWork;
import ru.microarch.delivery.application.types.UnitOfWorkFactory;
import ru.microarch.delivery.domain.courier.aggregate.Courier;
import ru.microarch.delivery.ports.CourierRepository;

import java.util.UUID;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class StopWorkHandlerTest {
    @Mock
    private CourierRepository courierRepository;

    private StopWorkHandler handler;


    @Mock
    private UnitOfWork mockUnitOfWork;

    private final UnitOfWorkFactory unitOfWorkFactory = () -> mockUnitOfWork;

    @BeforeEach
    public void setUp() {
        handler = new StopWorkHandler(courierRepository, unitOfWorkFactory);
    }

    @Test
    void testHandleStopWorkHandlerCommand() {
        // given
        StopWorkCommand command = new StopWorkCommand(UUID.randomUUID());
        Courier mockCourier = mock(Courier.class);

        when(courierRepository.get(command.courierId())).thenReturn(mockCourier);

        // when
        handler.handle(command);

        // then
        verify(mockCourier).stopWork();
        verify(mockUnitOfWork).submitForUpdate(mockCourier);
        verify(mockUnitOfWork).commit();
    }
}