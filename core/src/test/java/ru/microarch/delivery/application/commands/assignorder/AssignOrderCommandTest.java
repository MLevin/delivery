package ru.microarch.delivery.application.commands.assignorder;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.microarch.delivery.application.types.UnitOfWork;
import ru.microarch.delivery.application.types.UnitOfWorkFactory;
import ru.microarch.delivery.domain.courier.aggregate.Courier;
import ru.microarch.delivery.domain.courier.aggregate.CourierStatus;
import ru.microarch.delivery.domain.courier.aggregate.Transport;
import ru.microarch.delivery.domain.order.aggregate.Order;
import ru.microarch.delivery.domain.order.aggregate.OrderStatus;
import ru.microarch.delivery.domain.sharedkernel.Location;
import ru.microarch.delivery.domain.sharedkernel.Weight;
import ru.microarch.delivery.domainservices.DispatchService;
import ru.microarch.delivery.ports.CourierRepository;
import ru.microarch.delivery.ports.OrderRepository;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AssignOrderCommandTest {

    @Mock
    private CourierRepository courierRepository;

    @Mock
    private OrderRepository orderRepository;
    @Spy
    private DispatchService dispatchService;

    @Mock
    private UnitOfWork mockUnitOfWork;

    private final UnitOfWorkFactory unitOfWorkFactory = () -> mockUnitOfWork;

    private AssignOrdersCommand assignOrderCommand;

    private AssignOrdersHandler handler;

    @BeforeEach
    public void setUp() {
        assignOrderCommand = new AssignOrdersCommand();
        handler = new AssignOrdersHandler(dispatchService, orderRepository, courierRepository, unitOfWorkFactory);
    }

    public Courier createCourier(CourierStatus status) {
        return Courier.builder()
                .id(UUID.fromString("123e4567-e89b-12d3-a456-426614174000"))
                .name("Turanga Leela")
                .transport(Transport.CAR)
                .location(new Location(3, 5))
                .status(status)
                .build();
    }

    @Test
    void whenConditionsAreMetThenOrderGetsAssigned() {
        // given
        var order = new Order(UUID.randomUUID(), new Location(5, 5), new Weight(1));
        var courier = createCourier(CourierStatus.READY);

        when(courierRepository.getAllReady()).thenReturn(List.of(courier));
        when(orderRepository.getAllCreated()).thenReturn(List.of(order));

        // when
        handler.handle(assignOrderCommand);

        // then
        assertAll("Check the Order have been assigned",
                () -> assertEquals(courier.getId(), order.getCourierId(), "The courier id did not match"),
                () -> assertEquals(OrderStatus.ASSIGNED, order.getStatus(), "The status did not match"),
                () -> verify(dispatchService).dispatch(order, List.of(courier)),
                () -> verify(mockUnitOfWork).submitForUpdate(order),
                () -> verify(mockUnitOfWork).submitForUpdate(courier),
                () -> verify(mockUnitOfWork).commit()
        );
    }
}