package ru.microarch.delivery.application.commands.startwork;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class StartWorkCommandTest {

    @Test
    void testValidCommand() {
        UUID courierId = UUID.randomUUID();
        Assertions.assertDoesNotThrow(() -> new StartWorkCommand(courierId));
    }

    @Test
    void whenNullCourierIdThenThrow() {
        var exception = assertThrows(NullPointerException.class, () -> new StartWorkCommand(null));
        assertEquals("Courier ID cannot be null.", exception.getMessage());
    }
}