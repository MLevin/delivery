package ru.microarch.delivery.application.commands.movecouriers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.microarch.delivery.application.types.UnitOfWork;
import ru.microarch.delivery.application.types.UnitOfWorkFactory;
import ru.microarch.delivery.domain.courier.aggregate.Courier;
import ru.microarch.delivery.domain.courier.aggregate.Transport;
import ru.microarch.delivery.domain.order.aggregate.Order;
import ru.microarch.delivery.domain.order.aggregate.OrderStatus;
import ru.microarch.delivery.domain.sharedkernel.Location;
import ru.microarch.delivery.domain.sharedkernel.Weight;
import ru.microarch.delivery.ports.CourierRepository;
import ru.microarch.delivery.ports.OrderRepository;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MoveCouriersHandlerTest {

    @Mock
    private CourierRepository courierRepository;
    @Mock
    private OrderRepository orderRepository;

    @Mock
    private UnitOfWork mockUnitOfWork;
    private final UnitOfWorkFactory unitOfWorkFactory = () -> mockUnitOfWork;

    private MoveCouriersHandler handler;

    private Courier courier;

    private Order order;

    @BeforeEach
    public void setUp() {
        handler = new MoveCouriersHandler(courierRepository, orderRepository, unitOfWorkFactory);
    }

    void prepareCourierAndOrder(Location orderLocation) {
        courier = new Courier("Victor", Transport.BICYCLE);
        courier.startWork();

        order = new Order(UUID.randomUUID(), orderLocation, new Weight(1));
        order.assign(courier);

        when(courierRepository.getAllBusy()).thenReturn(List.of(courier));
        when(orderRepository.getAllAssigned()).thenReturn(List.of(order));
    }

    @Test
    void testOrderCompletionUponReachingDestination() {
        prepareCourierAndOrder(new Location(2,1));  // initial location is x=1, y=1

        handler.handle(new MoveCouriersCommand());

        assertEquals(OrderStatus.COMPLETED, order.getStatus(), "Order should be completed when destination is reached");
    }

    @Test
    void testOrderStatusNotCompletedWhenNotReachedDestination() {
        prepareCourierAndOrder(new Location(9, 9));  // A far location

        handler.handle(new MoveCouriersCommand());

        assertNotEquals(OrderStatus.COMPLETED, order.getStatus(), "Order should not be completed if not reached destination");
    }

    @Test
    void verifyRepositoriesUpdatedCorrectly() {
        prepareCourierAndOrder(new Location(5, 5));

        handler.handle(new MoveCouriersCommand());

        verify(mockUnitOfWork).submitForUpdate(refEq(courier));
        verify(mockUnitOfWork).commit();
    }
}