package ru.microarch.delivery.application.commands.createorder;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.microarch.delivery.application.types.UnitOfWork;
import ru.microarch.delivery.application.types.UnitOfWorkFactory;
import ru.microarch.delivery.domain.order.aggregate.Order;
import ru.microarch.delivery.domain.sharedkernel.Location;
import ru.microarch.delivery.domain.sharedkernel.Weight;
import ru.microarch.delivery.ports.GeoClient;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.refEq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CreateOrderHandlerTest {

    @Mock
    private GeoClient geoClient;

    @Mock
    private UnitOfWork mockUnitOfWork;
    private final UnitOfWorkFactory unitOfWorkFactory = () -> mockUnitOfWork;

    @Test
    void testHandleValidCommand() {
        // given
        CreateOrderHandler handler = new CreateOrderHandler(geoClient, unitOfWorkFactory);
        UUID basketId = UUID.randomUUID();
        String address = "123 Main St";
        int weight = 10;
        CreateOrderCommand command = new CreateOrderCommand(basketId, address, weight);

        // Mock setup for GeoClient
        Location mockLocation = new Location(5, 5);
        when(geoClient.getLocation(address)).thenReturn(mockLocation);

        // when
        handler.handle(command);

        // then
        Order expectedOrder = new Order(
                basketId,
                new Location(5, 5),
                new Weight(weight)
        );


        verify(geoClient).getLocation(address);
        verify(mockUnitOfWork).submitForCreate(refEq(expectedOrder, "domainEvents"));
        verify(mockUnitOfWork).commit();
    }
}