package ru.microarch.delivery.application.commands.createorder;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.UUID;

class CreateOrderCommandTest {

    @Test
    void testValidCommand() {
        UUID basketId = UUID.randomUUID();
        String address = "123 Apple St";
        int weight = 10;

        // Assert that no exception is thrown
        Assertions.assertDoesNotThrow(() -> new CreateOrderCommand(basketId, address, weight));
    }

    @Test
    void testNullBasketId() {
        assertException(NullPointerException.class,
                () -> new CreateOrderCommand(null, "123 Apple St", 10),
                "Basket ID cannot be null");
    }

    @Test
    void testNullAddress() {
        assertException(NullPointerException.class,
                () -> new CreateOrderCommand(UUID.randomUUID(), null, 10),
                "Address cannot be null");
    }

    @Test
    void testEmptyAddress() {
        assertException(IllegalArgumentException.class,
                () -> new CreateOrderCommand(UUID.randomUUID(), " ", 10),
                "Address cannot be empty");
    }

    @Test
    void testWeightLessThanOne() {
        assertException(IllegalArgumentException.class,
                () -> new CreateOrderCommand(UUID.randomUUID(), "123 Apple St", 0),
                "Weight must be greater than 0");
    }

    // Helper function to test exceptions and their messages
    private void assertException(Class<? extends Throwable> expectedException,
                                 Executable executable,
                                 String expectedMessage) {
        Exception exception = (Exception) Assertions.assertThrows(expectedException, executable);
        Assertions.assertTrue(exception.getMessage().contains(expectedMessage), "Expected message to contain: " + expectedMessage);
    }

    // Optional: additional test to check for minimal positive edge case for weight
    @Test
    void testMinimalPositiveWeight() {
        UUID basketId = UUID.randomUUID();
        String address = "123 Apple St";
        int weight = 1;

        // Assert that no exception is thrown for the minimum positive weight
        Assertions.assertDoesNotThrow(() -> new CreateOrderCommand(basketId, address, weight));
    }
}