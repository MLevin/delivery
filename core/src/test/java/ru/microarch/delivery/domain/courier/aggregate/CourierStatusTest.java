package ru.microarch.delivery.domain.courier.aggregate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CourierStatusTest {

    @Test
    void testGetName() {
        Assertions.assertEquals("NotAvailable", CourierStatus.NOT_AVAILABLE.getName());
        assertEquals("Ready", CourierStatus.READY.getName());
        assertEquals("Busy", CourierStatus.BUSY.getName());
    }

    @Test
    void testFromString() {
        assertEquals(CourierStatus.NOT_AVAILABLE, CourierStatus.fromString("NotAvailable"));
        assertEquals(CourierStatus.READY, CourierStatus.fromString("Ready"));
        assertEquals(CourierStatus.BUSY, CourierStatus.fromString("Busy"));
    }

    @Test
    void testFromStringCaseInsensitive() {
        assertEquals(CourierStatus.NOT_AVAILABLE, CourierStatus.fromString("notavailable"));
        assertEquals(CourierStatus.READY, CourierStatus.fromString("ready"));
        assertEquals(CourierStatus.BUSY, CourierStatus.fromString("BUSY"));
    }

    @Test
    void testFromStringInvalidValue() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> CourierStatus.fromString("invalid"));
        assertTrue(exception.getMessage().contains("No constant with a name invalid found in Enum CourierStatus"));
    }

    @Test
    void testFromStringNullValue() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> CourierStatus.fromString(null));
        assertTrue(exception.getMessage().contains("No constant with a name null found in Enum CourierStatus"));
    }

    @Test
    void testFromIdInvalidValue() {
        int invalidId = Integer.MAX_VALUE;
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> CourierStatus.fromId(invalidId));
        String expectedMsg = String.format("No constant with id=%d found in Enum CourierStatus", invalidId);
        assertEquals(expectedMsg, exception.getMessage());
    }

    @Test
    void testFromId() {
        CourierStatus courierStatus = CourierStatus.fromId(1);
        assertEquals(CourierStatus.NOT_AVAILABLE, courierStatus);
    }

}