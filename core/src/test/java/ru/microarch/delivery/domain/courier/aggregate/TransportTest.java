package ru.microarch.delivery.domain.courier.aggregate;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import ru.microarch.delivery.domain.sharedkernel.Weight;

import java.util.UUID;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class TransportTest {

    private static final Transport TEST_TRANSPORT = Transport.BICYCLE;
    private static final Weight TEST_TRANSPORT_CAPACITY = TEST_TRANSPORT.getCapacity();

    @Test
    void whenWeightIsEqualToCapacityThenAbleToCary() {
        assertTrue(TEST_TRANSPORT.isCapableOfCaring(TEST_TRANSPORT_CAPACITY));
    }

    @Test
    void whenWeightToHeavyThenUnableToCary() {
        var heavierWeigh = new Weight(TEST_TRANSPORT_CAPACITY.kilograms() + 1);
        assertFalse(TEST_TRANSPORT.isCapableOfCaring(heavierWeigh));
    }

    @Test
    void whenIdsAreEqualThenEntitiesAreEqual() {
        UUID uuid = UUID.randomUUID();
        var transport1 = new Transport(1, "Transport 1", 1, new Weight(1));
        var transport2 = new Transport(1, "Transport 2", 2, new Weight(2));

        assertEquals(transport1, transport2);
    }

    @Test
    void whenIdsAreDifferentThenEntitiesAreNotEqual() {
        var weight = new Weight(1);
        var transport1 = new Transport(1, "Transport 1", 1, weight);
        var transport2 = new Transport(2, "Transport 1", 1, weight);

        assertNotEquals(transport1, transport2);
    }

    @Test
    void testFromName() {
        assertEquals(Transport.BICYCLE, Transport.fromName("Bicycle"));
    }

    @Test
    void testFromNameIsCaseInsensitive() {
        assertEquals(Transport.BICYCLE, Transport.fromName("biCycle"));
    }

    private static Stream<Transport> provideAllTransports() {
        return Stream.of(Transport.BICYCLE, Transport.PEDESTRIAN, Transport.SCOOTER, Transport.CAR);
    }

    @ParameterizedTest
    @MethodSource("provideAllTransports")
    void testFromNameForAllTransports(Transport transport) {
        assertEquals(transport, Transport.fromName(transport.getName()));
    }

    @Test
    void whenNotValidNameThenFromNameThrows() {
        var exception = assertThrows(IllegalArgumentException.class, () -> Transport.fromName("Unknown"));
        assertEquals("No transport found with the name: Unknown", exception.getMessage());
    }


    @ParameterizedTest
    @MethodSource("provideAllTransports")
    void testFromIdForAllTransports(Transport transport) {
        assertEquals(transport, Transport.fromId(transport.getId()));
    }

    @Test
    void whenIdNotFoundThenFromIdThrows() {
        var id = 2341234;
        var exception = assertThrows(IllegalArgumentException.class,
                () -> Transport.fromId(id));
        assertEquals("No transport found with the id: " + id, exception.getMessage());
    }
}