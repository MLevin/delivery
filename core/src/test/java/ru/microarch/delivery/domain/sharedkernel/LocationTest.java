package ru.microarch.delivery.domain.sharedkernel;

import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class LocationTest {

    /**
     * Location - это координата на доске, она состоит из X (горизонталь) и Y (вертикаль).
     * <ul>
     *     <li>Должна быть возможность рассчитать расстояние между двумя Location.</li>
     *     <li>Расстояние между Location - это количество ходов по X и Y, которое необходимо сделать курьеру,
     *     чтобы достигнуть точки (шаги по X + шаги по Y).</li>
     *     <li>Минимально возможная для установки координата 1,1.</li>
     *     <li>Максимально возможная для установки координата 10,10.</li>
     *     <li>Две координаты равны, если их X и Y равны.</li>
     *     <li>Нельзя изменять объект Location после создания.</li>
     * </ul>
     */

    public static final int VALID_X = Location.MAX_X - 1;
    public static final int VALID_Y = Location.MAX_Y - 1;

    @Test
    void validLocationCreation() {
        Location location = new Location(VALID_X, VALID_Y);
        assertNotNull(location, "Location should be created");
        assertEquals(VALID_X, location.x(), "X coordinate should match the expected value");
        assertEquals(VALID_Y, location.y(), "Y coordinate should match the expected value");
    }

    @Test
    void checkMinMaxCoordinates() {
        assertEquals(10, Location.MAX_X);
        assertEquals(10, Location.MAX_Y);
        assertEquals(1, Location.MIN_X);
        assertEquals(1, Location.MIN_Y);
    }

    private static Stream<Arguments> provideInvalidCoordinates() {
        return Stream.of(
                Arguments.of(Location.MIN_X - 1, VALID_Y),
                Arguments.of(VALID_X, Location.MIN_Y - 1),
                Arguments.of(Location.MAX_X + 1, VALID_Y),
                Arguments.of(VALID_X, Location.MAX_Y + 1)
        );
    }

    @ParameterizedTest
    @MethodSource("provideInvalidCoordinates")
    void coordinatesExceedsRangeThenException(int x, int y) {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> new Location(x, y));

        // Checking the error message
        String expectedMessage = String.format("X coordinates must be between %d and %d, and Y coordinates must be between %d and %d",
                Location.MIN_X, Location.MAX_X, Location.MIN_Y, Location.MAX_Y);
        assertTrue(exception.getMessage().contains(expectedMessage), "Error message does not match expected output.");
    }

    @Test
    void distanceBetweenCalculatesCorrectly() {
        Location startLocation = new Location(1, 1);
        Location targetLocation = new Location(4, 5);
        assertEquals(7, Location.calculateDistance(startLocation, targetLocation));
    }

    @Test
    void distanceToCalculatesCorrectly() {
        Location startLocation = new Location(3, 4);
        Location targetLocation = new Location(1, 1);
        assertEquals(5, startLocation.distanceTo(targetLocation));
        assertEquals(5, targetLocation.distanceTo(startLocation)); // Checking if distance is symmetric
    }

    @Test
    void locationsHavingSameCoordinatesAreEqual() {
        Location loc1 = new Location(3, 4);
        Location loc2 = new Location(3, 4);
        Location loc3 = new Location(4, 3);

        assertEquals(loc1, loc2);
        assertNotEquals(loc1, loc3);
    }

    @RepeatedTest(1000)
    void randomLocationNeverExceedsBoundaries() {
        // Test the randomLocation method multiple times to ensure it consistently produces valid results
        Location location = Location.randomLocation();
        assertTrue(location.x() >= Location.MIN_X && location.x() <= Location.MAX_X,
                "X coordinate is out of bounds: " + location.x());
        assertTrue(location.y() >= Location.MIN_Y && location.y() <= Location.MAX_Y,
                "Y coordinate is out of bounds: " + location.y());
    }

    @Test
    void testMoveBy() {
        Location location = new Location(3, 4);
        Location result = location.moveBy(2, 3);

        assertEquals(5, result.x());
        assertEquals(7, result.y());
    }

    @Test
    void testMoveByWhenHitsBoundary() {
        Location location = new Location(Location.MAX_X - 1, Location.MAX_Y - 1);
        Location result = location.moveBy(2, 2);

        assertEquals(Location.MAX_X, result.x());
        assertEquals(Location.MAX_Y, result.y());
    }

    @Test
    void testMoveByWhenNegativeValues() {
        Location location = new Location(5, 5);
        Location result = location.moveBy(-2, -3);

        assertEquals(3, result.x());
        assertEquals(2, result.y());
    }

    @Test
    void testMoveByWhenHittingNegativeBoundary() {
        Location location = new Location(Location.MIN_X + 1, Location.MIN_Y + 1);
        Location result = location.moveBy(-2, -2);

        assertEquals(Location.MIN_X, result.x());
        assertEquals(Location.MIN_Y, result.y());
    }
}