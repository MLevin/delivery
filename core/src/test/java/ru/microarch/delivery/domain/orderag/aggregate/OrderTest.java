package ru.microarch.delivery.domain.orderag.aggregate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import ru.microarch.delivery.domain.courier.aggregate.Courier;
import ru.microarch.delivery.domain.courier.aggregate.Transport;
import ru.microarch.delivery.domain.order.aggregate.Order;
import ru.microarch.delivery.domain.order.aggregate.OrderStatus;
import ru.microarch.delivery.domain.sharedkernel.Location;
import ru.microarch.delivery.domain.sharedkernel.Weight;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class OrderTest {

    private Order createOrder() {
        return new Order(UUID.randomUUID(), new Location(2, 2), new Weight(1));
    }

    @Test
    void whenCreatedThenFieldInitialized() {
        Location location = new Location(2, 2);
        Weight weight = new Weight(1);
        var id = UUID.randomUUID();

        Order order = new Order(id, location, weight);
        assertEquals(id, order.getId());
        assertEquals(location, order.getTargetLocation());
        assertEquals(weight, order.getWeight());
    }

    @Test
    void whenCreatedThenStatusIsCreated() {
        Order order = createOrder();
        assertEquals(OrderStatus.CREATED, order.getStatus());
    }

    @Test
    void onlyAssignedOrderCanBeCompleted() {
        Order order = createOrder();
        var exception = assertThrows(IllegalStateException.class, order::complete);
        assertEquals("An order that has not been assigned cannot be completed", exception.getMessage());

    }

    @Test
    void whenIdAreEqualThenOrdersEqual() {
        UUID uuid = UUID.randomUUID();
        Order order1 = new Order(uuid, new Location(2, 2), new Weight(6));
        Order order2 = new Order(uuid, new Location(5, 6), new Weight(1));
        assertEquals(order1, order2);
    }

    @Nested
    class AssignedOrderTests {
        private Order order;
        private Courier courier;

        @BeforeEach
        void setUp() {
            order = createOrder();
            courier = new Courier("John Doe", Transport.BICYCLE);
            courier.startWork();
            order.assign(courier);
        }

        @Test
        void whenAssignOrderThenStatusIsAssigned() {
            assertEquals(OrderStatus.ASSIGNED, order.getStatus());
        }

        @Test
        void whenAssignOrderThenRememberCourierId() {
            assertEquals(courier.getId(), order.getCourierId());
        }

        @Test
        void whenCompletedThenStatusIsCompleted() {
            order.complete();
            assertEquals(OrderStatus.COMPLETED, order.getStatus());
        }
    }
}