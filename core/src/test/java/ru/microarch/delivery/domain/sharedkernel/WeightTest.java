package ru.microarch.delivery.domain.sharedkernel;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class WeightTest {

    /**
     * Weight - это вес, выраженный в килограммах.
     * <ul>
     *   <li>Нельзя задать нулевой или отрицательный вес.</li>
     *   <li>Нужно иметь возможность сравнивать Weight с другим.</li>
     *   <li>Два Weight равны, если их значение в килограммах равны, обеспечьте функционал проверки на эквивалентность.</li>
     *   <li>Нельзя изменять объект Weight после создания.</li>
     * </ul>
     */

    @Test
    void validWeightCreation() {
        Weight weight = new Weight(10);
        assertNotNull(weight);
        assertEquals(10, weight.kilograms());
    }

    @Test
    void cannotCreateWeightWithZeroValue() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> new Weight(0));
        assertEquals("Weight must be greater than 0", exception.getMessage());
    }

    @Test
    void cannotCreateWeightWithNegativeValue() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> new Weight(-1));
        assertEquals("Weight must be greater than 0", exception.getMessage());
    }

    @Test
    void weightsAreEqualIfValuesAreTheSame() {
        Weight weight1 = new Weight(5);
        Weight weight2 = new Weight(5);
        assertTrue(weight1.equals(weight2));
    }

    @Test
    void weightsAreNotEqualIfValuesAreDifferent() {
        Weight weight1 = new Weight(5);
        Weight weight2 = new Weight(6);
        assertFalse(weight1.equals(weight2));
    }

    @Test
    void weightIsGreaterThanOther() {
        Weight weight1 = new Weight(10);
        Weight weight2 = new Weight(5);
        assertTrue(weight1.isGreaterThan(weight2));
        assertFalse(weight2.isGreaterThan(weight1));
    }

    @Test
    void weightIsLessThanOther() {
        Weight weight1 = new Weight(5);
        Weight weight2 = new Weight(10);
        assertTrue(weight1.isLessThan(weight2));
        assertFalse(weight2.isLessThan(weight1));
    }

    @Test
    void weightIsGreaterThanOrEqualToOtherWhenGreater() {
        Weight weight1 = new Weight(10);
        Weight weight2 = new Weight(5);
        assertTrue(weight1.isGreaterThanOrEqualTo(weight2));
        assertFalse(weight2.isGreaterThanOrEqualTo(weight1));
    }

    @Test
    void weightIsGreaterThanOrEqualToOtherWhenEqual() {
        Weight weight1 = new Weight(5);
        Weight weight2 = new Weight(5);
        assertTrue(weight1.isGreaterThanOrEqualTo(weight2));
    }

    @Test
    void weightIsLessThanOrEqualToOtherWhenLess() {
        Weight weight1 = new Weight(5);
        Weight weight2 = new Weight(10);
        assertTrue(weight1.isLessThanOrEqualTo(weight2));
        assertFalse(weight2.isLessThanOrEqualTo(weight1));
    }

    @Test
    void weightIsLessThanOrEqualToOtherWhenEqual() {
        Weight weight1 = new Weight(5);
        Weight weight2 = new Weight(5);
        assertTrue(weight1.isLessThanOrEqualTo(weight2));
    }

    @Test
    void whenSortingWeightsThenReturnAscendingOrder() {
        // Creating an unsorted list of weights
        List<Weight> weights = Arrays.asList(
                new Weight(10),
                new Weight(5),
                new Weight(20),
                new Weight(15)
        );

        // Sorting the list
        weights.sort(null);

        // Asserting the list is sorted in ascending order by checking the kilograms directly
        for (int i = 0; i < weights.size() - 1; i++) {
            assertTrue(weights.get(i).kilograms() <= weights.get(i + 1).kilograms());
        }
    }
}