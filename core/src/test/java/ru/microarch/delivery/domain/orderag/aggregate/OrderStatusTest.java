package ru.microarch.delivery.domain.orderag.aggregate;

import org.junit.jupiter.api.Test;
import ru.microarch.delivery.domain.order.aggregate.OrderStatus;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class OrderStatusTest {

    @Test
    void testGetName() {
        assertEquals("Created", OrderStatus.CREATED.getName());
        assertEquals("Assigned", OrderStatus.ASSIGNED.getName());
        assertEquals("Completed", OrderStatus.COMPLETED.getName());
    }

    @Test
    void testFromString() {
        assertEquals(OrderStatus.CREATED, OrderStatus.fromString("Created"));
        assertEquals(OrderStatus.ASSIGNED, OrderStatus.fromString("Assigned"));
        assertEquals(OrderStatus.COMPLETED, OrderStatus.fromString("Completed"));
    }

    @Test
    void testFromStringCaseInsensitive() {
        assertEquals(OrderStatus.CREATED, OrderStatus.fromString("created"));
        assertEquals(OrderStatus.ASSIGNED, OrderStatus.fromString("aSSigned"));
        assertEquals(OrderStatus.COMPLETED, OrderStatus.fromString("COMPLETED"));
    }

    @Test
    void testFromStringInvalidValue() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> OrderStatus.fromString("invalid"));
        assertTrue(exception.getMessage().contains("No constant with text invalid found in Enum OrderStatus"));
    }

    @Test
    void testFromStringNullValue() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> OrderStatus.fromString(null));
        assertTrue(exception.getMessage().contains("No constant with text null found in Enum OrderStatus"));
    }
}