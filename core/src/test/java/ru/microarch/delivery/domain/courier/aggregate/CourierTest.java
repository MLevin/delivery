package ru.microarch.delivery.domain.courier.aggregate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.microarch.delivery.domain.sharedkernel.Location;
import ru.microarch.delivery.domain.sharedkernel.Weight;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class CourierTest {

    private final static Transport TEST_TRANSPORT = Transport.BICYCLE;

    private final static Location TEST_TARGET_LOCATION = new Location(5, 5);

    private final static Location ONE_STEP_TARGET_LOCATION = new Location(2, 2);

    private static final String TEST_NAME = "John Doe";

    private Courier courier;

    @BeforeEach
    void setUp() {
        courier = createCourier();
    }

    private Courier createCourier() {
        return new Courier(TEST_NAME, TEST_TRANSPORT);
    }

    @Test
    void whenCreatedThenNameParametersSetCorrectly() {
        assertAll("Should check all fields",
                () -> assertEquals(TEST_NAME, courier.getName(), "Name did not match"),
                () -> assertEquals(TEST_TRANSPORT, courier.getTransport(), "Transport did not match"),
                () -> assertNotNull(courier.getId(), "The id must not be null"),
                () -> Assertions.assertEquals(CourierStatus.NOT_AVAILABLE, courier.getStatus(), "The status should be NOT_AVAILABLE"),
                () -> assertEquals(Courier.DEFAULT_LOCATION, courier.getLocation(), "The location differs from default")
        );
    }

    @Test
    void checkDefaultLocation() {
        assertEquals(new Location(1, 1), Courier.DEFAULT_LOCATION);
    }

    @Test
    void whenStartWorkThenStatusReady() {
        courier.startWork();
        assertEquals(CourierStatus.READY, courier.getStatus());
    }

    @Test
    void givenAlreadyWorkingWhenStartWorkThenThrow() {
        courier.startWork();
        var exception = assertThrows(IllegalStateException.class, courier::startWork);
        assertEquals("The courier is already working", exception.getMessage());
    }

    @Test
    void whenStopWorkingThenStatusNotAvailable() {
        courier.startWork();

        assertNotEquals(CourierStatus.NOT_AVAILABLE, courier.getStatus());
        courier.stopWork();
        assertEquals(CourierStatus.NOT_AVAILABLE, courier.getStatus());
    }

    private Courier createCourierReadyToMove() {
        courier.startWork();
        courier.assignOrder();
        return courier;
    }

    @Test
    void whenBusyThenCantStopWork() {
        Courier courier = createCourierReadyToMove();
        var exception = assertThrows(IllegalStateException.class, courier::stopWork);
        assertEquals("The courier is busy and cannot stop work", exception.getMessage());
    }

    @Test
    void whenAssignedOrderThenStatusBusy() {
        Courier courier = createCourierReadyToMove();
        assertEquals(CourierStatus.BUSY, courier.getStatus());
    }

    @Test
    void whenBusyThenCantTakeOrder() {
        Courier courier = createCourierReadyToMove();
        var exception = assertThrows(IllegalStateException.class, courier::assignOrder);
        assertEquals("The courier is not ready and cannot take an order", exception.getMessage());
    }

    @Test
    void whenNotAvailableThenCantTakeOrder() {
        var exception = assertThrows(IllegalStateException.class, courier::assignOrder);
        assertEquals("The courier is not ready and cannot take an order", exception.getMessage());
    }

    @Test
    void testCalculateTimeToPoint() {
        assertEquals(2, courier.getTransport().getSpeed());
        assertEquals(4, courier.calculateTimeToPoint(TEST_TARGET_LOCATION));
    }

    @Test
    void whenMoveEnoughStepsThenReachTargetLocation() {
        Courier courier = createCourierReadyToMove();

        int stepCount = courier.calculateTimeToPoint(TEST_TARGET_LOCATION);

        for (int i = 0; i < stepCount; i++) {
            courier.move(TEST_TARGET_LOCATION);
        }
        assertEquals(TEST_TARGET_LOCATION, courier.getLocation());
    }

    @Test
    void whenTargetDistanceLessThanOneStepThenReachTargetLocation() {
        Courier courier = createCourierReadyToMove();

        var initialX = courier.getLocation().x();
        var initialY = courier.getLocation().y();

        // Checking the precondition
        int distanceToTarget = 1;
        var step = courier.getTransport().getSpeed();
        var targetLocation = new Location(initialX + distanceToTarget, initialY);
        assertTrue(step > distanceToTarget);

        courier.move(targetLocation);
        assertEquals(targetLocation, courier.getLocation());
    }

    @Test
    void whenNotBusyThenCantMove() {
        assertNotEquals(CourierStatus.BUSY, courier.getStatus());

        var exception = assertThrows(IllegalStateException.class, () -> courier.move(TEST_TARGET_LOCATION));
        assertEquals("The courier can't move before an order is assigned", exception.getMessage());
    }

    @Test
    void whenReachTargetThenStatusIsReady() {
        Courier courier = createCourierReadyToMove();

        courier.move(ONE_STEP_TARGET_LOCATION);

        assertEquals(ONE_STEP_TARGET_LOCATION, courier.getLocation());

        assertEquals(CourierStatus.READY, courier.getStatus());
    }

    @Test
    void whenMoveThenTakeOneStep() {
        Courier courier = createCourierReadyToMove();

        var targetLocation = new Location(5, 5);
        var initialLocation = courier.getLocation();

        courier.move(targetLocation);

        var speed = courier.getTransport().getSpeed();
        var coveredDistance = initialLocation.distanceTo(courier.getLocation());

        assertEquals(speed, coveredDistance);
    }

    @Test
    void whenIdAreEqualThenCouriersEqual() {
        UUID uuid = UUID.randomUUID();
        Courier courier1 = new Courier(uuid, "John", Transport.BICYCLE, new Location(2, 4), CourierStatus.BUSY, 0);
        Courier courier2 = new Courier(uuid, "Max", Transport.PEDESTRIAN, new Location(2, 4), CourierStatus.BUSY, 0);
        assertEquals(courier1, courier2);
    }

    @Test
    void whenWeightIsToHeavyThenCantCary() {
        var weight = new Weight(2000);
        Courier courier = createCourier();

        assertFalse(courier.getTransport().isCapableOfCaring(weight));
        assertFalse(courier.isCapableOfCaring(weight));
    }

    @Test
    void whenWeightIsLightThenCanCary() {
        var weight = new Weight(1);
        Courier courier = createCourier();

        assertTrue(courier.getTransport().isCapableOfCaring(weight));
        assertTrue(courier.isCapableOfCaring(weight));
    }
}