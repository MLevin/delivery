package ru.microarch.delivery.domainservices;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.microarch.delivery.domain.courier.aggregate.Courier;
import ru.microarch.delivery.domain.courier.aggregate.CourierStatus;
import ru.microarch.delivery.domain.courier.aggregate.Transport;
import ru.microarch.delivery.domain.order.aggregate.Order;
import ru.microarch.delivery.domain.order.aggregate.OrderStatus;
import ru.microarch.delivery.domain.sharedkernel.Location;
import ru.microarch.delivery.domain.sharedkernel.Weight;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class DispatchServiceTest {

    private Order order;

    @BeforeEach
    void init() {
        order = new Order(UUID.randomUUID(), new Location(9, 9), new Weight(1));
    }


    private final DispatchService dispatchService = new DispatchService();

    @Test
    void whenDispatchThenSuccess() {
        // given
        var courier = new Courier("c 1", Transport.CAR);
        courier.startWork();

        assertEquals(OrderStatus.CREATED, order.getStatus());
        assertEquals(CourierStatus.READY, courier.getStatus());
        assertTrue(courier.isCapableOfCaring(order.getWeight()));

        // when
        Optional<Courier> chosenCourier = dispatchService.dispatch(order, List.of(courier));

        // then
        assertTrue(chosenCourier.isPresent());
        assertEquals(courier, chosenCourier.get());
    }

    @Test
    void whenNoReadyCourierThenChoseNobody() {
        // given
        var courier = new Courier("c 1", Transport.CAR);
        assertNotEquals(CourierStatus.READY, courier.getStatus());

        // when
        Optional<Courier> chosenCourier = dispatchService.dispatch(order, List.of(courier));

        // then
        assertFalse(chosenCourier.isPresent());
    }

    @Test
    void whenCouriersCantCaryWeighThenChoseNobody() {
        // given
        var heavyOrder = new Order(UUID.randomUUID(), new Location(9, 9), new Weight(10));
        var courier = new Courier("c 1", Transport.CAR);
        courier.startWork();

        assertFalse(courier.isCapableOfCaring(heavyOrder.getWeight()));

        // when
        Optional<Courier> chosenCourier = dispatchService.dispatch(heavyOrder, List.of(courier));

        // then
        assertFalse(chosenCourier.isPresent());
    }

    @Test
    void whenChoosingThenWinsCourierWithLesserDeliveryTime() {
        // given
        var fastCourier = new Courier("c 1", Transport.CAR);
        var slowCourier1 = new Courier("c 2", Transport.PEDESTRIAN);
        var slowCourier2 = new Courier("c 2", Transport.BICYCLE);
        fastCourier.startWork();
        slowCourier1.startWork();
        slowCourier2.startWork();

        //when
        Optional<Courier> chosenCourier = dispatchService.dispatch(order, List.of(slowCourier1, fastCourier, slowCourier2));

        // then
        assertTrue(chosenCourier.isPresent());
        assertEquals(fastCourier, chosenCourier.get());
    }

    @Test
    void whenDispatchOrderAlreadyInWorkThenThrow() {
        // given
        var courier = new Courier("c 1", Transport.CAR);
        var couriers = List.of(courier);
        courier.startWork();
        order.assign(courier);

        assertNotEquals(OrderStatus.CREATED, order.getStatus());

        // then
        var exception = assertThrows(IllegalStateException.class,
                () -> dispatchService.dispatch(order, couriers));
        assertEquals("Order is already being handled. Order id=" + order.getId(), exception.getMessage());
    }
}