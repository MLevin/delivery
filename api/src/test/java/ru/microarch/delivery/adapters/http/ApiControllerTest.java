package ru.microarch.delivery.adapters.http;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Testcontainers;
import ru.microarch.delivery.application.queries.getactiveorders.OrderDto;
import ru.microarch.delivery.application.queries.getallcouriers.CourierDto;
import ru.microarch.delivery.application.queries.shareddto.LocationDto;
import ru.microarch.delivery.domain.courier.aggregate.CourierStatus;
import ru.microarch.delivery.ports.CourierRepository;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@SpringBootTest
@AutoConfigureMockMvc
@Testcontainers
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Sql({"/20240423_01_courier_create.sql", "/02_courier_truncate.sql", "/03_courier_insert.sql",
        "/20240423_02_order_create.sql", "/01_order_truncate.sql", "/04_order_insert.sql"})
class ApiControllerTest {

    private static PostgreSQLContainer<?> postgresqlContainer;

    static {
        postgresqlContainer = new PostgreSQLContainer<>("postgres:latest")
                .withUsername("postgres")
                .withPassword("password")
                .withDatabaseName("testdb");
        postgresqlContainer.start();
    }


    @DynamicPropertySource
    static void postgresqlProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgresqlContainer::getJdbcUrl);
        registry.add("spring.datasource.username", postgresqlContainer::getUsername);
        registry.add("spring.datasource.password", postgresqlContainer::getPassword);
    }

    @Autowired
    private CourierRepository courierRepository;


    private static final OrderDto CREATED_ORDER = new OrderDto(UUID.fromString("123e4567-e89b-12d3-a456-426614174000"),
            new LocationDto(5, 5));

    private static final OrderDto ASSIGNED_ORDER = new OrderDto(UUID.fromString("123e4567-e89b-12d3-a456-426614174002"),
            new LocationDto(7, 7));

    private static final CourierDto READY_COURIER = new CourierDto(UUID.fromString("123e4567-e89b-12d3-a456-426614174000"),
            "Turanga Leela",
            new LocationDto(3, 5));

    private static final CourierDto NOT_AVAILABLE_COURIER = new CourierDto(UUID.fromString("123e4567-e89b-12d3-a456-426614174001"),
            "Bender Bending Rodríguez",
            new LocationDto(7, 3));

    @Autowired
    private MockMvc mockMvc;


    @Test
    void testGetActiveOrders() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/orders/active")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.orders[0].id").value(CREATED_ORDER.id().toString()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.orders[0].location.x").value(CREATED_ORDER.location().x()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.orders[0].location.y").value(CREATED_ORDER.location().y()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.orders[1].id").value(ASSIGNED_ORDER.id().toString()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.orders[1].location.x").value(ASSIGNED_ORDER.location().x()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.orders[1].location.y").value(ASSIGNED_ORDER.location().y()))
                // This assertion checks that the 'orders' array size is exactly 2
                .andExpect(MockMvcResultMatchers.jsonPath("$.orders", Matchers.hasSize(2)));
    }

    @Test
    void testGetAllCouriers() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/couriers/")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.couriers[0].id").value(READY_COURIER.id().toString()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.couriers[0].name").value(READY_COURIER.name()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.couriers[0].location.x").value(READY_COURIER.location().x()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.couriers[0].location.y").value(READY_COURIER.location().y()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.couriers[1].id").value(NOT_AVAILABLE_COURIER.id().toString()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.couriers[1].name").value(NOT_AVAILABLE_COURIER.name()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.couriers[1].location.x").value(NOT_AVAILABLE_COURIER.location().x()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.couriers[1].location.y").value(NOT_AVAILABLE_COURIER.location().y()))
                // This assertion checks that the 'couriers' array size is exactly 2
                .andExpect(MockMvcResultMatchers.jsonPath("$.couriers", Matchers.hasSize(2)));
    }

    @Test
    void testStartWork() throws Exception {
        UUID courierId = NOT_AVAILABLE_COURIER.id();
        assertNotEquals(CourierStatus.READY, courierRepository.get(courierId).getStatus());

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/couriers/{courierId}/start-work", courierId)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());

        assertEquals(CourierStatus.READY, courierRepository.get(courierId).getStatus());
    }

    @Test
    void testStopWork() throws Exception {
        UUID courierId = READY_COURIER.id();
        assertNotEquals(CourierStatus.NOT_AVAILABLE, courierRepository.get(courierId).getStatus());

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/couriers/{courierId}/stop-work", courierId)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());

        assertEquals(CourierStatus.NOT_AVAILABLE, courierRepository.get(courierId).getStatus());
    }
}