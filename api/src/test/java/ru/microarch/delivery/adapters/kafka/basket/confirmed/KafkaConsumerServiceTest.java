package ru.microarch.delivery.adapters.kafka.basket.confirmed;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;
import ru.microarch.delivery.CommandBus;
import ru.microarch.delivery.application.commands.createorder.CreateOrderCommand;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;


@SpringBootTest
@Testcontainers
class KafkaConsumerServiceTest {
    @Container
    public static KafkaContainer kafka = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:5.4.3"));

    @DynamicPropertySource
    static void kafkaProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.kafka.producer.bootstrap-servers", kafka::getBootstrapServers);
        registry.add("spring.kafka.consumer.bootstrap-servers", kafka::getBootstrapServers);
    }

    @MockBean
    private CommandBus commandBus;

    @Autowired
    private KafkaTemplate<String, BasketConfirmedIntegrationEvent> kafkaTemplate;

    @Captor
    private ArgumentCaptor<CreateOrderCommand> commandCaptor;


    @Test
    void whenKafkaReceiveCreateOrderMessageThenCommandGetsDispatched() throws Exception {
        // Arrange
        BasketConfirmedIntegrationEvent event = new BasketConfirmedIntegrationEvent();
        event.setBasketId(UUID.randomUUID().toString());
        event.setAddress("Test Address");
        event.setWeight(10);

        var key = UUID.randomUUID().toString();

        // Act
        kafkaTemplate.send("basket.confirmed", key, event).get();

        // Assert
        verify(commandBus, timeout(1000)).dispatch(commandCaptor.capture());
        CreateOrderCommand dispatchedCommand = commandCaptor.getValue();

        assertAll("Received kafka message in the proper format",
                () -> assertEquals(UUID.fromString(event.getBasketId()), dispatchedCommand.basketId()),
                () -> assertEquals(event.getAddress(), dispatchedCommand.address()),
                () -> assertEquals(event.getWeight(), dispatchedCommand.weight())
        );
    }
}