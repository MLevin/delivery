package ru.microarch.delivery.adapters.background.jobs;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.microarch.delivery.CommandBus;
import ru.microarch.delivery.application.commands.assignorder.AssignOrdersCommand;

import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;

@SpringBootTest
class MoveCouriersJobTest {

    @MockBean
    private CommandBus commandBus;

    @Test
    void testJobGetsExecuted() throws InterruptedException {
        verify(commandBus, timeout(5000)).dispatch(isA(AssignOrdersCommand.class));
    }
}