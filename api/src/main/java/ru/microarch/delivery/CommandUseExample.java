package ru.microarch.delivery;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.microarch.delivery.application.commands.startwork.StartWorkCommand;
import ru.microarch.delivery.application.queries.getallcouriers.CourierDto;
import ru.microarch.delivery.application.queries.getallcouriers.GetAllCouriersQuery;

import java.util.List;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class CommandUseExample {

    private final CommandBus commandBus;


    public void useCommandWithArgument() {
        var courierId = UUID.randomUUID();
        commandBus.dispatch(new StartWorkCommand(courierId));
    }

    public void useQuery() {
        List<CourierDto> allCouriers = commandBus.dispatch(new GetAllCouriersQuery());
    }
}
