package ru.microarch.delivery.adapters.background.jobs;


import org.quartz.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QuartzConfig {

    @Value("${quartz.job.assign-orders.interval}")
    private int assignOrdersInterval;

    @Value("${quartz.job.move-couriers.interval}")
    private int moveCouriersInterval;

    @Value("${quartz.job.process-outbox.interval}")
    private int processOutboxInterval;

    // Generic method to create job details
    private <T extends Job> JobDetail createJobDetail(Class<T> jobClass, String jobName, String groupName) {
        return JobBuilder.newJob(jobClass)
                .withIdentity(jobName, groupName)
                .storeDurably()
                .build();
    }

    // Generic method to create triggers
    private Trigger createTrigger(JobDetail jobDetail, int intervalInSeconds, String triggerName, String groupName) {
        return TriggerBuilder.newTrigger()
                .forJob(jobDetail)
                .withIdentity(triggerName, groupName)
                .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                        .withIntervalInSeconds(intervalInSeconds)
                        .repeatForever())
                .build();
    }

    @Bean
    public JobDetail assignOrdersJobDetail() {
        return createJobDetail(AssignOrdersJob.class, "assignOrdersJob", "jobsGroup");
    }

    @Bean
    public Trigger assignOrdersJobTrigger(JobDetail assignOrdersJobDetail) {
        return createTrigger(assignOrdersJobDetail, assignOrdersInterval, "assignOrdersJobTrigger", "triggersGroup");
    }

    @Bean
    public JobDetail moveCouriersJobDetail() {
        return createJobDetail(MoveCouriersJob.class, "moveCouriersJob", "jobsGroup");
    }

    @Bean
    public Trigger moveCouriersJobTrigger(JobDetail moveCouriersJobDetail) {
        return createTrigger(moveCouriersJobDetail, moveCouriersInterval, "moveCouriersJobTrigger", "triggersGroup");
    }

    @Bean
    public JobDetail processOutboxMessagesJobDetail() {
        return createJobDetail(ProcessOutboxMessagesJob.class, "processOutboxMessagesJob", "jobsGroup");
    }

    @Bean
    public Trigger processOutboxMessagesJobTrigger(JobDetail processOutboxMessagesJobDetail) {
        return createTrigger(processOutboxMessagesJobDetail, processOutboxInterval, "processOutboxMessagesTrigger", "triggersGroup");
    }
}