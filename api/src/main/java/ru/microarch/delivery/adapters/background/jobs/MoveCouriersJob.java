package ru.microarch.delivery.adapters.background.jobs;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import ru.microarch.delivery.CommandBus;
import ru.microarch.delivery.application.commands.movecouriers.MoveCouriersCommand;

@RequiredArgsConstructor
@DisallowConcurrentExecution
@Slf4j
public class MoveCouriersJob extends AbstractJob {

    private final CommandBus commandBus;

    @Override
    protected void executeLogged(JobExecutionContext context) throws Exception {
        commandBus.dispatch(new MoveCouriersCommand());
    }
}
