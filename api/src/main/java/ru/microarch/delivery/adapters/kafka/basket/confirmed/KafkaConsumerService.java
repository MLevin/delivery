package ru.microarch.delivery.adapters.kafka.basket.confirmed;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import ru.microarch.delivery.CommandBus;
import ru.microarch.delivery.application.commands.createorder.CreateOrderCommand;

import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
public class KafkaConsumerService {

    private final CommandBus commandBus;


    @KafkaListener(topics = "${kafka.consumer.topic}", groupId = "${spring.kafka.consumer.group-id}")
    public void listen(ConsumerRecord<String, BasketConfirmedIntegrationEvent> consumerRecord) {
        BasketConfirmedIntegrationEvent event = consumerRecord.value();
        String key = consumerRecord.key();
        try {
            processMessage(key, event);
        } catch (Exception e) {
            log.error("Error processing message with key: {} and value: {}", key, event, e);
        }
    }

    private void processMessage(String key, BasketConfirmedIntegrationEvent event) {
        log.info("Processing message with key: {} and value: {}", key, event);
        commandBus.dispatch(new CreateOrderCommand(UUID.fromString(event.getBasketId()), event.getAddress(), event.getWeight()));
    }
}