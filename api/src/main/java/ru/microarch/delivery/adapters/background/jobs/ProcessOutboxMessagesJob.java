package ru.microarch.delivery.adapters.background.jobs;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Value;
import ru.microarch.delivery.adapters.postgres.OutboxService;


@RequiredArgsConstructor
@DisallowConcurrentExecution
@Slf4j
public class ProcessOutboxMessagesJob extends AbstractJob {

    private final OutboxService outboxService;

    @Value("${quartz.job.process-outbox.batch-size}")
    private Integer batchSize;

    @Override
    protected void executeLogged(JobExecutionContext context) throws Exception {
        outboxService.processMessages(batchSize);
    }
}
