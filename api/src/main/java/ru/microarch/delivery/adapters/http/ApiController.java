package ru.microarch.delivery.adapters.http;

import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import ru.microarch.delivery.CommandBus;
import ru.microarch.delivery.adapters.http.api.ApiApi;
import ru.microarch.delivery.adapters.http.model.*;
import ru.microarch.delivery.application.commands.createorder.CreateOrderCommand;
import ru.microarch.delivery.application.commands.startwork.StartWorkCommand;
import ru.microarch.delivery.application.commands.stopwork.StopWorkCommand;
import ru.microarch.delivery.application.queries.getactiveorders.GetActiveOrdersQuery;
import ru.microarch.delivery.application.queries.getallcouriers.GetAllCouriersQuery;
import ru.microarch.delivery.application.queries.shareddto.LocationDto;

import java.util.List;
import java.util.UUID;

@CrossOrigin
@RestController
@RequiredArgsConstructor
public class ApiController implements ApiApi {

    private final CommandBus commandBus;

    @Override
    public ResponseEntity<Void> createOrder(CreateOrderRequest request) {
        commandBus.dispatch(new CreateOrderCommand(request.getBasketId(), request.getAddress(), request.getWeight()));
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<ActiveOrdersResponse> getActiveOrders() {
        List<Order> orders = commandBus.dispatch(new GetActiveOrdersQuery()).stream()
                .map(dto -> new Order(dto.id(), convert(dto.location())))
                .toList();
        return ResponseEntity.ok(new ActiveOrdersResponse(orders));
    }

    @Override
    public ResponseEntity<AllCouriersResponse> getAllCouriers() {
        List<Courier> couriers = commandBus.dispatch(new GetAllCouriersQuery()).stream()
                .map(dto -> new Courier(dto.id(), dto.name(), convert(dto.location())))
                .toList();
        return ResponseEntity.ok(new AllCouriersResponse(couriers));
    }

    private Location convert(LocationDto dto) {
        return new Location(dto.x(), dto.y());
    }

    @Override
    public ResponseEntity<Void> startWork(@NotNull UUID courierId) {
        commandBus.dispatch(new StartWorkCommand(courierId));
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> stopWork(@NotNull UUID courierId) {
        commandBus.dispatch(new StopWorkCommand(courierId));
        return ResponseEntity.ok().build();
    }
}
