/**
 * {@link ru.microarch.delivery.adapters api.adapters} Входящие адаптеры (Primary (driving) Adapters),
 * напрямую вызывают Use Cases ядра приложения
 */

package ru.microarch.delivery.adapters;