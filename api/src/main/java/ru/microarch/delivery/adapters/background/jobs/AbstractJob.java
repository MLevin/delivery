package ru.microarch.delivery.adapters.background.jobs;

import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

@Slf4j
public abstract class AbstractJob implements Job {

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        log.info("Job ** {} ** starting @ {}", context.getJobDetail().getKey().getName(), context.getFireTime());
        try {
            executeLogged(context);
        } catch (Exception e) {
            log.error("Error processing job with name: {}", context.getJobDetail().getKey().getName(), e);
            // Optional: rethrow the exception to indicate job execution failure
            throw new JobExecutionException(e);
        } finally {
            log.info("Job ** {} ** completed. Next job scheduled @ {}", context.getJobDetail().getKey().getName(), context.getNextFireTime());
        }
    }

    protected abstract void executeLogged(JobExecutionContext context) throws Exception;
}