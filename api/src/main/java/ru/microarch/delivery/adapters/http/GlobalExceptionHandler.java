package ru.microarch.delivery.adapters.http;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.NoSuchElementException;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<Object> handleNotFoundException(Exception e) {
        return prepareResponse(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = NoSuchElementException.class)
    public ResponseEntity<Object> handleNotFoundException(NoSuchElementException e) {
        return prepareResponse(e, HttpStatus.NOT_FOUND);
    }

    private ResponseEntity<Object> prepareResponse(Exception e, HttpStatus status) {
        return new ResponseEntity<>("An error occurred: " + e.getMessage(), status);
    }
}
