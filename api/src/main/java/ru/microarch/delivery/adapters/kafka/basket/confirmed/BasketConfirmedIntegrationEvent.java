package ru.microarch.delivery.adapters.kafka.basket.confirmed;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class BasketConfirmedIntegrationEvent {
    @JsonProperty("BasketId")
    private String basketId;

    @JsonProperty("Address")
    private String address;

    @JsonProperty("Weight")
    private int weight;
}