package ru.microarch.delivery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.microarch.delivery.application.types.CommandHandler;
import ru.microarch.delivery.application.types.Request;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class CommandBus {
    private final Map<Class<? extends Request<?>>, CommandHandler<? extends Request<?>, ?>> handlers;

    @Autowired
    public CommandBus(List<CommandHandler<? extends Request<?>, ?>> handlerList) {
        handlers = new HashMap<>();
        handlerList.forEach(handler -> handlers.put(handler.getCommandType(), handler));
    }

    public <C extends Request<T>, T> T dispatch(C command) {
        @SuppressWarnings("unchecked")
        CommandHandler<C, T> handler = (CommandHandler<C, T>) handlers.get(command.getClass());
        if (handler == null) {
            throw new IllegalArgumentException("No handler found for command type: " + command.getClass().getName());
        }
        return handler.handle(command);
    }
}