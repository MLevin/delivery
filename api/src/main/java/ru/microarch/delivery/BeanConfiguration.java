package ru.microarch.delivery;


import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.microarch.delivery.application.commands.assignorder.AssignOrdersHandler;
import ru.microarch.delivery.application.commands.createorder.CreateOrderHandler;
import ru.microarch.delivery.application.commands.movecouriers.MoveCouriersHandler;
import ru.microarch.delivery.application.commands.startwork.StartWorkHandler;
import ru.microarch.delivery.application.commands.stopwork.StopWorkHandler;
import ru.microarch.delivery.application.domainevent.handlers.OrderAssignedDomainEventHandler;
import ru.microarch.delivery.application.domainevent.handlers.OrderCompletedDomainEventHandler;
import ru.microarch.delivery.application.domainevent.handlers.OrderCreatedDomainEventHandler;
import ru.microarch.delivery.application.types.UnitOfWorkFactory;
import ru.microarch.delivery.domainservices.DispatchService;
import ru.microarch.delivery.ports.BusProducer;
import ru.microarch.delivery.ports.CourierRepository;
import ru.microarch.delivery.ports.GeoClient;
import ru.microarch.delivery.ports.OrderRepository;

@Configuration
@RequiredArgsConstructor
public class BeanConfiguration {

    @Bean
    public StartWorkHandler startWorkHandler(CourierRepository courierRepository, UnitOfWorkFactory unitOfWorkFactory) {
        return new StartWorkHandler(courierRepository, unitOfWorkFactory);
    }

    @Bean
    public StopWorkHandler stopWorkHandler(CourierRepository courierRepository, UnitOfWorkFactory unitOfWorkFactory) {
        return new StopWorkHandler(courierRepository, unitOfWorkFactory);
    }

    @Bean
    public MoveCouriersHandler moveCouriersHandler(CourierRepository courierRepository,
                                                   OrderRepository orderRepository,
                                                   UnitOfWorkFactory unitOfWorkFactory) {
        return new MoveCouriersHandler(courierRepository, orderRepository, unitOfWorkFactory);
    }

    @Bean
    public CreateOrderHandler createOrderHandler(OrderRepository orderRepository, GeoClient geoClient,
                                                 UnitOfWorkFactory unitOfWorkFactory) {
        return new CreateOrderHandler(geoClient, unitOfWorkFactory);
    }

    @Bean
    public DispatchService dispatchService() {
        return new DispatchService();
    }

    @Bean
    public AssignOrdersHandler assignOrdersHandler(
            DispatchService dispatchService,
            OrderRepository orderRepository,
            CourierRepository courierRepository,
            UnitOfWorkFactory unitOfWorkFactory) {
        return new AssignOrdersHandler(dispatchService, orderRepository, courierRepository, unitOfWorkFactory);

    }

    @Bean
    public OrderCreatedDomainEventHandler orderCreatedDomainEventHandler(BusProducer busProducer) {
        return new OrderCreatedDomainEventHandler(busProducer);
    }

    @Bean
    public OrderCompletedDomainEventHandler orderCompletedDomainEventHandler(BusProducer busProducer) {
        return new OrderCompletedDomainEventHandler(busProducer);
    }

    @Bean
    public OrderAssignedDomainEventHandler orderAssignedDomainEventHandler(BusProducer busProducer) {
        return new OrderAssignedDomainEventHandler(busProducer);
    }

}
