CREATE TABLE IF NOT EXISTS couriers
(
    id         UUID PRIMARY KEY,
    name       VARCHAR(255),
    location_x INT,
    location_y INT,
    transport  INT,
    status     INT NOT NULL,
    created_at TIMESTAMP DEFAULT now() NOT NULL,
    modified_at TIMESTAMP DEFAULT now() NOT NULL,
    version INTEGER   DEFAULT 0     NOT NULL
);