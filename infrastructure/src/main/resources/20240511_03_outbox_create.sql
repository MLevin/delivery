CREATE TABLE IF NOT EXISTS outbox
(
    id           UUID PRIMARY KEY        NOT NULL,
    type         VARCHAR                 NOT NULL,
    content      jsonb                   NOT NULL,
    is_processed BOOLEAN   DEFAULT FALSE,
    created_at   TIMESTAMP DEFAULT now() NOT NULL,
    modified_at  TIMESTAMP,
    version      INTEGER   DEFAULT 0     NOT NULL,
    error        TEXT
);