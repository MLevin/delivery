CREATE TABLE IF NOT EXISTS orders
(
    id         UUID PRIMARY KEY,
    courier_id UUID,
    location_x INTEGER,
    location_y INTEGER,
    weight     INTEGER NOT NULL,
    status     VARCHAR NOT NULL,
    created_at TIMESTAMP DEFAULT now() NOT NULL,
    modified_at TIMESTAMP DEFAULT now() NOT NULL,
    version INTEGER   DEFAULT 0     NOT NULL
);

CREATE INDEX IF NOT EXISTS idx_courier_id ON orders (courier_id);