package ru.microarch.delivery.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.microarch.delivery.application.types.EventHandler;
import ru.microarch.delivery.util.primitives.DomainEvent;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class EventBus {

    private final Map<Class<? extends DomainEvent>, EventHandler<? extends DomainEvent>> handlers;

    @Autowired
    public EventBus(List<EventHandler<? extends DomainEvent>> handlerList) {
        handlers = new HashMap<>();
        handlerList.forEach(handler -> handlers.put(handler.getEventType(), handler));
    }

    public <T extends DomainEvent> void dispatch(T event) {
        @SuppressWarnings("unchecked")
        EventHandler<T> handler = (EventHandler<T>) handlers.get(event.getClass());
        if (handler == null) {
            throw new IllegalArgumentException("No handler found for event type: " + event.getClass().getName());
        }
        handler.handle(event);
    }
}
