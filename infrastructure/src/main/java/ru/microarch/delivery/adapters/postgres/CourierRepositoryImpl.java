package ru.microarch.delivery.adapters.postgres;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.microarch.delivery.adapters.postgres.jpa.CourierJpaRepository;
import ru.microarch.delivery.adapters.postgres.mapper.CourierMapper;
import ru.microarch.delivery.domain.courier.aggregate.Courier;
import ru.microarch.delivery.domain.courier.aggregate.CourierStatus;
import ru.microarch.delivery.ports.CourierRepository;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

import static ru.microarch.delivery.adapters.postgres.constant.Messages.NOT_FOUND_WITH_ID;

@Service
@RequiredArgsConstructor
public class CourierRepositoryImpl implements CourierRepository {

    private final CourierJpaRepository jpaRepository;

    private final CourierMapper mapper;

    @Override
    @Transactional
    public Courier add(Courier courier) {
        var orderEntity = mapper.toDbEntity(courier);
        return mapper.fromDbEntity(jpaRepository.save(orderEntity));
    }

    @Override
    @Transactional
    public void update(Courier courier) {
        var existingEntity = jpaRepository.findById(courier.getId())
                .orElseThrow(() -> new NoSuchElementException(String.format(NOT_FOUND_WITH_ID, courier.getId())));
        var courierEntity = mapper.toDbEntity(courier);
        courierEntity.setCreatedAt(existingEntity.getCreatedAt());
        jpaRepository.save(courierEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public Courier get(UUID courierId) {
        return jpaRepository.findById(courierId)
                .map(mapper::fromDbEntity)
                .orElseThrow(() -> new NoSuchElementException(String.format(NOT_FOUND_WITH_ID, courierId)));
    }

    @Override
    @Transactional(readOnly = true)
    public List<Courier> getAllReady() {
        return mapper.fromDbEntities(jpaRepository.findByStatus(CourierStatus.READY));
    }

    @Override
    @Transactional(readOnly = true)
    public List<Courier> getAllBusy() {
        return mapper.fromDbEntities(jpaRepository.findByStatus(CourierStatus.BUSY));
    }

    @Override
    public Class<Courier> getAggregateType() {
        return Courier.class;
    }
}
