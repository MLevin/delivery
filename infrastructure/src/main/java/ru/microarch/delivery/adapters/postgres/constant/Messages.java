package ru.microarch.delivery.adapters.postgres.constant;

public final class Messages {

    private Messages() {
    }

    public static final String NOT_FOUND_WITH_ID = "Not found entity with id=%s";
}
