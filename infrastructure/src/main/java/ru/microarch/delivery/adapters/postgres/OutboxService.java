package ru.microarch.delivery.adapters.postgres;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ru.microarch.delivery.adapters.postgres.entity.OutboxMessage;
import ru.microarch.delivery.adapters.postgres.jpa.OutboxRepository;
import ru.microarch.delivery.util.EventBus;
import ru.microarch.delivery.util.primitives.DomainEvent;

import java.util.List;


@Service
@RequiredArgsConstructor
public class OutboxService {

    private final OutboxRepository outboxRepository;

    private final EventBus eventBus;

    private final ObjectMapper mapper;

    @Transactional
    public void processMessages(int number) {
        PageRequest pageable = PageRequest.of(0, number);
        List<OutboxMessage> unprocessedMessages = outboxRepository.findByIsProcessedFalseOrderByCreatedAtAsc(pageable);
        if (unprocessedMessages.isEmpty()) {
            return;
        }

        for (OutboxMessage outboxMessage : unprocessedMessages) {
            try {
                var domainEvent = convertToDomainEvent(outboxMessage);
                eventBus.dispatch(domainEvent);
                outboxMessage.setIsProcessed(true);
            } catch (Exception e) {
                String errorMessage = String.format("Exception of type %s with a message: %s",
                        e.getClass().getSimpleName(),
                        e.getMessage());
                outboxMessage.setError(errorMessage);
            }
        }
        outboxRepository.saveAll(unprocessedMessages);
    }

    private DomainEvent convertToDomainEvent(OutboxMessage outboxMessage) {
        try {
            String fullClassName = "ru.microarch.delivery.domain.order.aggregate.events." + outboxMessage.getType();
            Class<?> domainEventClass = Class.forName(fullClassName);
            return (DomainEvent) mapper.readValue(outboxMessage.getContent(), domainEventClass);
        } catch (JsonProcessingException | ClassNotFoundException e) {
            throw new RuntimeException("Failed to deserialize domain event", e);
        }
    }
}
