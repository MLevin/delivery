package ru.microarch.delivery.adapters.utility;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.microarch.delivery.adapters.postgres.jpa.OutboxRepository;
import ru.microarch.delivery.application.types.UnitOfWork;
import ru.microarch.delivery.application.types.UnitOfWorkFactory;
import ru.microarch.delivery.util.primitives.Aggregate;
import ru.microarch.delivery.util.primitives.AggregateRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class UnitOfWorkFactoryImpl implements UnitOfWorkFactory {

    private final Map<Class<?>, AggregateRepository<? extends Aggregate>> repositoryMap;

    private final OutboxRepository outboxRepository;

    private final ObjectMapper mapper;

    @Autowired
    public UnitOfWorkFactoryImpl(List<AggregateRepository<? extends Aggregate>> repositories,
                                 OutboxRepository outboxRepository,
                                 ObjectMapper mapper) {
        this.repositoryMap = new HashMap<>();
        repositories.forEach(repo -> repositoryMap.put(repo.getAggregateType(), repo));
        this.outboxRepository = outboxRepository;
        this.mapper = mapper;
    }

    @Override
    public UnitOfWork create() {
        return new UnitOfWorkImpl(repositoryMap, outboxRepository, mapper);
    }
}
