package ru.microarch.delivery.adapters.grpc;


import io.grpc.ManagedChannel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.microarch.delivery.adapters.grpc.java.GeoGrpc;
import ru.microarch.delivery.adapters.grpc.java.Grpc;
import ru.microarch.delivery.domain.sharedkernel.Location;
import ru.microarch.delivery.ports.GeoClient;

import javax.annotation.PreDestroy;

@Component
@RequiredArgsConstructor
public class GeoClientImpl implements GeoClient {

    private final GeoGrpc.GeoBlockingStub blockingStub;


    @Override
    public Location getLocation(String address) {
        Grpc.GetGeolocationRequest request = Grpc.GetGeolocationRequest.newBuilder()
                .setAddress(address)
                .build();

        Grpc.GetGeolocationReply reply;
        try {
            reply = blockingStub.getGeolocation(request);
        } catch (RuntimeException e) {
            throw new IllegalStateException("Failed to retrieve geolocation", e);
        }

        Grpc.Location grpcLocation = reply.getLocation();
        return new Location(grpcLocation.getX(), grpcLocation.getY());
    }

    @PreDestroy
    public void shutdown() {
        ((ManagedChannel) blockingStub.getChannel()).shutdownNow();
    }
}