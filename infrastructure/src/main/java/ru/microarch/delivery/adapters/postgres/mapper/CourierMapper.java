package ru.microarch.delivery.adapters.postgres.mapper;


import org.mapstruct.Mapper;
import ru.microarch.delivery.adapters.postgres.entity.CourierEntity;
import ru.microarch.delivery.adapters.postgres.entity.LocationEmbedding;
import ru.microarch.delivery.domain.courier.aggregate.Courier;
import ru.microarch.delivery.domain.sharedkernel.Location;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CourierMapper {

    CourierEntity toDbEntity(Courier courier);

    Courier fromDbEntity(CourierEntity courierEntity);

    LocationEmbedding toDbEntity(Location location);

    Location fromDbEntity(LocationEmbedding locationEntity);

    List<Courier> fromDbEntities(List<CourierEntity> entities);

    default CourierEntity updateEntity(Courier source, CourierEntity target) {
        target.setName(source.getName());
        target.setTransport(source.getTransport());
        target.setLocation(toDbEntity(source.getLocation()));
        target.setStatus(source.getStatus());
        target.setVersion(source.getVersion());
        return target;
    }
}
