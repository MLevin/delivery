package ru.microarch.delivery.adapters.postgres.entity;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.microarch.delivery.domain.order.aggregate.OrderStatus;

import java.util.UUID;

@Entity
@Table(name = "orders")
@NoArgsConstructor
@Getter
@Setter
public class OrderEntity extends BaseEntity {

    @Id
    private UUID id;

    @Column(name = "courier_id")
    private UUID courierId;

    @Embedded
    private LocationEmbedding targetLocation;

    @Column(name = "weight", nullable = false)
    private Integer weight;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private OrderStatus status;
}
