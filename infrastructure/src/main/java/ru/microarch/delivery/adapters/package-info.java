/**
 * {@link ru.microarch.delivery.adapters infrastructure.adapters} Исходящие адаптеры (Secondary (driven) Adapters)
 * реализуют абстрактные порты ядра приложения
 */

package ru.microarch.delivery.adapters;