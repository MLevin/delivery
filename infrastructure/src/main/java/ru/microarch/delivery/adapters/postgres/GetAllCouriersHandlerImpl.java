package ru.microarch.delivery.adapters.postgres;

import org.jdbi.v3.core.Jdbi;
import org.springframework.stereotype.Component;
import ru.microarch.delivery.application.queries.getallcouriers.CourierDto;
import ru.microarch.delivery.application.queries.getallcouriers.GetAllCouriersHandler;
import ru.microarch.delivery.application.queries.getallcouriers.GetAllCouriersQuery;
import ru.microarch.delivery.application.queries.shareddto.LocationDto;

import javax.sql.DataSource;
import java.util.List;
import java.util.UUID;

@Component
public class GetAllCouriersHandlerImpl extends GetAllCouriersHandler {

    private final Jdbi jdbi;

    public GetAllCouriersHandlerImpl(DataSource dataSource) {
        this.jdbi = Jdbi.create(dataSource);
    }

    @Override
    public List<CourierDto> handle(GetAllCouriersQuery command) {
        return jdbi.withHandle(handle ->
                handle.createQuery("SELECT id, name, location_x, location_y FROM couriers")
                        .map((rs, ctx) -> new CourierDto(
                                UUID.fromString(rs.getString("id")),
                                rs.getString("name"),
                                new LocationDto(rs.getInt("location_x"), rs.getInt("location_y"))
                        ))
                        .list()
        );
    }
}
