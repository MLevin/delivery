package ru.microarch.delivery.adapters.grpc;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.microarch.delivery.adapters.grpc.java.GeoGrpc;

@Configuration
public class GprcConfig {
    @Value("${grpc.geo-service.host}")
    private String host;

    @Value("${grpc.geo-service.port}")
    private int port;

    @Bean
    public GeoGrpc.GeoBlockingStub blockingStub() {
        ManagedChannel channel = ManagedChannelBuilder.forAddress(host, port)
                .usePlaintext() // Don't do this in production, use SSL/TLS configuration
                .build();
        return GeoGrpc.newBlockingStub(channel);
    }
}
