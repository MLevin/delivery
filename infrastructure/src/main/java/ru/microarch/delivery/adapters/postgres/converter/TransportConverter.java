package ru.microarch.delivery.adapters.postgres.converter;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import ru.microarch.delivery.domain.courier.aggregate.Transport;

@Converter(autoApply = true)
public class TransportConverter implements AttributeConverter<Transport, Integer> {

    @Override
    public Integer convertToDatabaseColumn(Transport transport) {
        if (transport == null) {
            return null;
        }
        return transport.getId();
    }

    @Override
    public Transport convertToEntityAttribute(Integer id) {
        if (id == null) {
            return null;
        }
        return Transport.fromId(id);
    }
}