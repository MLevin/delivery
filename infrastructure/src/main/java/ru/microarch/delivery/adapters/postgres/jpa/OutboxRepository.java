package ru.microarch.delivery.adapters.postgres.jpa;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.microarch.delivery.adapters.postgres.entity.OutboxMessage;

import java.util.List;
import java.util.UUID;

public interface OutboxRepository extends JpaRepository<OutboxMessage, UUID> {

    List<OutboxMessage> findByIsProcessedFalseOrderByCreatedAtAsc(Pageable pageable);

}
