package ru.microarch.delivery.adapters.kafka.order.status.changed;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderStatusChangedIntegrationEvent {

    @JsonProperty("OrderId")
    private String orderId;

    @JsonProperty("OrderStatus")
    private OrderStatus orderStatus;

    public enum OrderStatus {
        NONE(0),
        CREATED(1),
        ASSIGNED(2),
        COMPLETED(3);

        private final int value;

        OrderStatus(int value) {
            this.value = value;
        }

        @JsonValue
        public int getValue() {
            return value;
        }

        @JsonCreator
        public static OrderStatus forValue(int value) {
            return fromValue(value);
        }

        public static OrderStatus fromValue(int value) {
            for (OrderStatus status : values()) {
                if (status.getValue() == value) {
                    return status;
                }
            }
            throw new IllegalArgumentException("Unexpected value '" + value + "'");
        }
    }
}