package ru.microarch.delivery.adapters.postgres.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.microarch.delivery.adapters.postgres.converter.CourierStatusConverter;
import ru.microarch.delivery.adapters.postgres.converter.TransportConverter;
import ru.microarch.delivery.domain.courier.aggregate.CourierStatus;
import ru.microarch.delivery.domain.courier.aggregate.Transport;

import java.util.UUID;

@Entity
@Table(name = "couriers")
@NoArgsConstructor
@Getter
@Setter
public class CourierEntity extends BaseEntity {
    @Id
    private UUID id;

    @Column(name = "name")
    private String name;

    @Convert(converter = TransportConverter.class)
    @Column(name = "transport")
    private Transport transport;

    @Embedded
    private LocationEmbedding location;

    @Convert(converter = CourierStatusConverter.class)
    @Column(name = "status", nullable = false)
    private CourierStatus status;
}
