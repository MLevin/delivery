package ru.microarch.delivery.adapters.postgres.mapper;


import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.microarch.delivery.adapters.postgres.entity.LocationEmbedding;
import ru.microarch.delivery.adapters.postgres.entity.OrderEntity;
import ru.microarch.delivery.domain.order.aggregate.Order;
import ru.microarch.delivery.domain.sharedkernel.Location;

import java.util.List;

@Mapper(componentModel = "spring")
public interface OrderMapper {

    @Mapping(source = "weight.kilograms", target = "weight")
    OrderEntity toDbEntity(Order order);

    @Mapping(source = "weight", target = "weight.kilograms")
    Order fromDbEntity(OrderEntity orderEntity);

    LocationEmbedding toDbEntity(Location location);

    Location fromDbEntity(LocationEmbedding locationEntity);

    List<Order> fromDbEntities(List<OrderEntity> entities);

    default OrderEntity updateEntity(Order source, OrderEntity target) {
        target.setCourierId(source.getCourierId());
        target.setTargetLocation(toDbEntity(source.getTargetLocation()));
        target.setWeight(source.getWeight().kilograms());
        target.setStatus(source.getStatus());
        target.setVersion(source.getVersion());
        return target;
    }
}
