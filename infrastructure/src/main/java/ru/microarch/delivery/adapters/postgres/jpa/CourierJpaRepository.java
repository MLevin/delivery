package ru.microarch.delivery.adapters.postgres.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.microarch.delivery.adapters.postgres.entity.CourierEntity;
import ru.microarch.delivery.domain.courier.aggregate.CourierStatus;

import java.util.List;
import java.util.UUID;

public interface CourierJpaRepository extends JpaRepository<CourierEntity, UUID> {
    List<CourierEntity> findByStatus(CourierStatus ready);
}
