package ru.microarch.delivery.adapters.postgres.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.microarch.delivery.adapters.postgres.entity.OrderEntity;
import ru.microarch.delivery.domain.order.aggregate.OrderStatus;

import java.util.List;
import java.util.UUID;

public interface OrderJpaRepository extends JpaRepository<OrderEntity, UUID> {

    List<OrderEntity> findByStatus(OrderStatus status);

    List<OrderEntity> findByStatusNot(OrderStatus status);
}
