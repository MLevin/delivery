package ru.microarch.delivery.adapters.postgres.entity;


import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Embeddable
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LocationEmbedding {

    @Column(name = "location_x")
    private Integer x;

    @Column(name = "location_y")
    private Integer y;
}
