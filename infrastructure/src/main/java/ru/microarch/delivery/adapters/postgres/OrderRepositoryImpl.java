package ru.microarch.delivery.adapters.postgres;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.microarch.delivery.adapters.postgres.jpa.OrderJpaRepository;
import ru.microarch.delivery.adapters.postgres.mapper.OrderMapper;
import ru.microarch.delivery.domain.order.aggregate.Order;
import ru.microarch.delivery.domain.order.aggregate.OrderStatus;
import ru.microarch.delivery.ports.OrderRepository;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

import static ru.microarch.delivery.adapters.postgres.constant.Messages.NOT_FOUND_WITH_ID;


@Service
@RequiredArgsConstructor
public class OrderRepositoryImpl implements OrderRepository {

    private final OrderJpaRepository jpaRepository;

    private final OrderMapper mapper;

    @Override
    @Transactional
    public Order add(Order order) {
        var orderEntity = mapper.toDbEntity(order);
        return mapper.fromDbEntity(jpaRepository.save(orderEntity));
    }


    @Override
    @Transactional
    public void update(Order order) {
        var existingEntity = jpaRepository.findById(order.getId())
                .orElseThrow(() -> new NoSuchElementException(String.format(NOT_FOUND_WITH_ID, order.getId())));
        var orderEntity = mapper.toDbEntity(order);
        orderEntity.setCreatedAt(existingEntity.getCreatedAt());
        jpaRepository.save(orderEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public Order get(UUID orderId) {
        return jpaRepository.findById(orderId)
                .map(mapper::fromDbEntity)
                .orElseThrow(() -> new NoSuchElementException(String.format(NOT_FOUND_WITH_ID, orderId)));
    }

    @Override
    @Transactional(readOnly = true)
    public List<Order> getAllCreated() {
        return mapper.fromDbEntities(jpaRepository.findByStatus(OrderStatus.CREATED));
    }

    @Override
    @Transactional(readOnly = true)
    public List<Order> getAllAssigned() {
        return mapper.fromDbEntities(jpaRepository.findByStatus(OrderStatus.ASSIGNED));
    }

    @Override
    public Class<Order> getAggregateType() {
        return Order.class;
    }
}
