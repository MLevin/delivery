package ru.microarch.delivery.adapters.kafka.order.status.changed;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import ru.microarch.delivery.adapters.kafka.order.status.changed.OrderStatusChangedIntegrationEvent.OrderStatus;
import ru.microarch.delivery.domain.order.aggregate.events.OrderAssignedDomainEvent;
import ru.microarch.delivery.domain.order.aggregate.events.OrderCompletedDomainEvent;
import ru.microarch.delivery.domain.order.aggregate.events.OrderCreatedDomainEvent;
import ru.microarch.delivery.ports.BusProducer;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class KafkaProducer implements BusProducer {

    private final KafkaTemplate<String, OrderStatusChangedIntegrationEvent> kafkaTemplate;


    @Value("${kafka.producer.topic}")
    private String topic;


    @Override
    public void publishOrderCreatedDomainEvent(OrderCreatedDomainEvent event) {
        publish(event.getEventId(), event.getOrderId(), OrderStatus.CREATED);
    }

    @Override
    public void publishOrderAssignedDomainEvent(OrderAssignedDomainEvent event) {
        publish(event.getEventId(), event.getOrderId(), OrderStatus.ASSIGNED);
    }

    @Override
    public void publishOrderCompletedDomainEvent(OrderCompletedDomainEvent event) {
        publish(event.getEventId(), event.getOrderId(), OrderStatus.COMPLETED);
    }

    private void publish(UUID eventId, UUID orderId, OrderStatus orderStatus) {
        var key = eventId.toString();
        var value = new OrderStatusChangedIntegrationEvent(orderId.toString(), orderStatus);
        kafkaTemplate.send(topic, key, value);
    }
}
