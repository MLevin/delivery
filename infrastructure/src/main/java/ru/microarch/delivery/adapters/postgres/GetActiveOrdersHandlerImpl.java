package ru.microarch.delivery.adapters.postgres;

import org.jdbi.v3.core.Jdbi;
import org.springframework.stereotype.Component;
import ru.microarch.delivery.application.queries.getactiveorders.GetActiveOrdersHandler;
import ru.microarch.delivery.application.queries.getactiveorders.GetActiveOrdersQuery;
import ru.microarch.delivery.application.queries.getactiveorders.OrderDto;
import ru.microarch.delivery.application.queries.shareddto.LocationDto;

import javax.sql.DataSource;
import java.util.List;
import java.util.UUID;

@Component
public class GetActiveOrdersHandlerImpl extends GetActiveOrdersHandler {

    private final Jdbi jdbi;

    public GetActiveOrdersHandlerImpl(DataSource dataSource) {
        this.jdbi = Jdbi.create(dataSource);
    }

    @Override
    public List<OrderDto> handle(GetActiveOrdersQuery command) {
        return jdbi.withHandle(handle ->
                handle.createQuery("SELECT id, location_x, location_y FROM orders WHERE status <> 'COMPLETED'")
                        .map((rs, ctx) -> new OrderDto(
                                UUID.fromString(rs.getString("id")),
                                new LocationDto(rs.getInt("location_x"), rs.getInt("location_y"))
                        ))
                        .list()
        );
    }
}
