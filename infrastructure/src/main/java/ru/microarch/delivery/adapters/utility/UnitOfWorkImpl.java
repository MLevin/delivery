package ru.microarch.delivery.adapters.utility;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.microarch.delivery.adapters.postgres.entity.OutboxMessage;
import ru.microarch.delivery.adapters.postgres.jpa.OutboxRepository;
import ru.microarch.delivery.application.types.UnitOfWork;
import ru.microarch.delivery.util.primitives.Aggregate;
import ru.microarch.delivery.util.primitives.AggregateRepository;
import ru.microarch.delivery.util.primitives.DomainEvent;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
public class UnitOfWorkImpl implements UnitOfWork {

    private final Map<Class<?>, AggregateRepository<? extends Aggregate>> repositoryMap;

    private final List<Aggregate> aggregates = new ArrayList<>();

    private final List<Runnable> runnables = new ArrayList<>();

    private final OutboxRepository outboxRepository;

    private final ObjectMapper mapper;


    public <T extends Aggregate> void submitForCreate(T aggregate) {
        var repository = getRepository(aggregate);
        aggregates.add(aggregate);
        runnables.add(() -> repository.add(aggregate));
    }

    public <T extends Aggregate> void submitForUpdate(T aggregate) {
        var repository = getRepository(aggregate);
        aggregates.add(aggregate);
        runnables.add(() -> repository.update(aggregate));
    }

    private <T extends Aggregate> AggregateRepository<T> getRepository(T aggregate) {
        @SuppressWarnings("unchecked")
        var repository = (AggregateRepository<T>) repositoryMap.get(aggregate.getClass());
        if (repository == null) {
            throw new IllegalArgumentException("No repository found for aggregate: " + aggregate.getClass().getName());
        }
        return repository;
    }

    @Override
    public void submit(Runnable runnable) {
        runnables.add(runnable);
    }

    @Override
    @Transactional
    public void commit() {
        runnables.forEach(Runnable::run);
        processOutboxMessages();
    }

    @Transactional(propagation = Propagation.MANDATORY)
    protected void processOutboxMessages() {
        List<OutboxMessage> outboxMessages = aggregates.stream()
                .map(Aggregate::getDomainEvents)
                .flatMap(Collection::stream)
                .map(this::createOutboxMessage)
                .toList();

        if (!outboxMessages.isEmpty()) {
            outboxRepository.saveAll(outboxMessages);
        }
    }

    private OutboxMessage createOutboxMessage(DomainEvent event) {
        try {
            return OutboxMessage.builder()
                    .id(event.getEventId())
                    .type(event.getClass().getSimpleName())
                    .isProcessed(false)
                    .content(mapper.writeValueAsString(event))
                    .build();
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Failed to serialize domain event", e);
        }
    }
}
