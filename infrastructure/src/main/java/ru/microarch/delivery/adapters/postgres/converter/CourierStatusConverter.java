package ru.microarch.delivery.adapters.postgres.converter;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import ru.microarch.delivery.domain.courier.aggregate.CourierStatus;

@Converter(autoApply = true)
public class CourierStatusConverter implements AttributeConverter<CourierStatus, Integer> {
    @Override
    public Integer convertToDatabaseColumn(CourierStatus status) {
        if (status == null) {
            return null;
        }
        return status.getId();
    }

    @Override
    public CourierStatus convertToEntityAttribute(Integer id) {
        return CourierStatus.fromId(id);
    }
}
