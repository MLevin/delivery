package ru.microarch.delivery.adapters.postgres.entity;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;
import org.hibernate.annotations.ColumnTransformer;

import javax.annotation.Nullable;
import java.util.UUID;

@Entity
@Table(name = "outbox")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class OutboxMessage extends BaseEntity {

    @Id
    @NonNull
    @Column(name = "id")
    private UUID id;

    @NonNull
    @Column(name = "type")
    private String type;

    @NonNull
    @ColumnTransformer(write = "?::jsonb")
    @Column(name = "content", columnDefinition = "jsonb")
    private String content;

    @NonNull
    @Column(name = "is_processed")
    private Boolean isProcessed;

    @Nullable
    @Column(name = "error")
    private String error;
}
