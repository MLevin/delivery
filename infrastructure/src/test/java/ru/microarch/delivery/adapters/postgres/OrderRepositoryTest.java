package ru.microarch.delivery.adapters.postgres;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import ru.microarch.delivery.domain.order.aggregate.Order;
import ru.microarch.delivery.domain.order.aggregate.OrderStatus;
import ru.microarch.delivery.domain.sharedkernel.Location;
import ru.microarch.delivery.domain.sharedkernel.Weight;
import ru.microarch.delivery.ports.OrderRepository;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Sql({"/20240423_02_order_create.sql", "/01_order_truncate.sql", "/04_order_insert.sql"})
class OrderRepositoryTest extends BaseDatabaseTest {

    private static final UUID NOT_FOUND_ID = UUID.fromString("3d7b691e-8bb4-49b9-abc8-e0ccc1f8fd90");

    private static final Order CREATED_ORDER = Order.builder()
            .id(UUID.fromString("123e4567-e89b-12d3-a456-426614174000"))
            .courierId(UUID.fromString("123e4567-e89b-12d3-a456-426614174001"))
            .targetLocation(new Location(5, 5))
            .weight(new Weight(200))
            .status(OrderStatus.CREATED)
            .build();

    private static final Order ASSIGNED_ORDER = Order.builder()
            .id(UUID.fromString("123e4567-e89b-12d3-a456-426614174002"))
            .courierId(UUID.fromString("123e4567-e89b-12d3-a456-426614174003"))
            .targetLocation(new Location(7, 7))
            .weight(new Weight(150))
            .status(OrderStatus.ASSIGNED)
            .build();

    private final static Order COMPLETED_ORDER = Order.builder()
            .id(UUID.fromString("123e4567-e89b-12d3-a456-426614174004"))
            .courierId(UUID.fromString("123e4567-e89b-12d3-a456-426614174005"))
            .targetLocation(new Location(9, 9))
            .weight(new Weight(100))
            .status(OrderStatus.COMPLETED)
            .build();


    @Autowired
    private OrderRepository repository;


    @Test
    void whenGetByIdThenSuccess() {
        Order order = repository.get(CREATED_ORDER.getId());
        assertThat(order).usingRecursiveComparison().isEqualTo(CREATED_ORDER);
    }

    @Test
    void whenIdNotFoundThenThrow() {
        var exception = assertThrows(NoSuchElementException.class, () -> repository.get(NOT_FOUND_ID));
        assertEquals("Not found entity with id=" + NOT_FOUND_ID, exception.getMessage());
    }

    @Test
    void testAdd() {
        Order newOrder = Order.builder()
                .id(UUID.fromString("473b8123-c8e0-4f19-be38-7c2e3c037053"))
                .courierId(UUID.fromString("9c651258-c549-4e50-84bf-e20cc2d051ad"))
                .targetLocation(new Location(2, 6))
                .weight(new Weight(7))
                .status(OrderStatus.ASSIGNED)
                .build();

        Order returnedOrder = repository.add(newOrder);
        assertThat(newOrder).usingRecursiveComparison().isEqualTo(returnedOrder);

        Order orderFromDb = repository.get(newOrder.getId());
        assertThat(newOrder).usingRecursiveComparison().isEqualTo(orderFromDb);
    }

    @Test
    void testGetAllAssigned() {
        List<Order> allAssigned = repository.getAllAssigned();

        assertEquals(1, allAssigned.size());
        assertThat(allAssigned.get(0)).usingRecursiveComparison().isEqualTo(ASSIGNED_ORDER);
    }

    @Test
    void testGetAllNotAssigned() {
        List<Order> allNotAssigned = repository.getAllCreated();

        assertThat(allNotAssigned)
                .hasSize(1)
                .containsExactlyInAnyOrder(CREATED_ORDER);
    }

    @Test
    void testUpdate() {
        var existingId = COMPLETED_ORDER.getId();

        Order updatedOrder = Order.builder()
                .id(existingId)
                .courierId(UUID.fromString("15b95109-7bdf-4a49-bbba-8190f2baf85f"))
                .targetLocation(new Location(9, 1))
                .weight(new Weight(7))
                .status(OrderStatus.CREATED)
                .build();

        repository.update(updatedOrder);

        Order orderFromDb = repository.get(updatedOrder.getId());
        assertThat(orderFromDb).usingRecursiveComparison().ignoringFields("version").isEqualTo(updatedOrder);
    }

    @Test
    void whenUpdatedOrderNotFoundThenThrow() {
        Order updatedOrder = Order.builder()
                .id(NOT_FOUND_ID)
                .courierId(UUID.fromString("15b95109-7bdf-4a49-bbba-8190f2baf85f"))
                .targetLocation(new Location(9, 1))
                .weight(new Weight(7))
                .status(OrderStatus.CREATED)
                .build();

        var exception = assertThrows(NoSuchElementException.class, () -> repository.update(updatedOrder));
        assertEquals("Not found entity with id=" + NOT_FOUND_ID, exception.getMessage());
    }
}
