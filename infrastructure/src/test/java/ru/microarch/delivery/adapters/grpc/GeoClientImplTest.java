package ru.microarch.delivery.adapters.grpc;

import io.grpc.ManagedChannel;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import ru.microarch.delivery.adapters.grpc.java.GeoGrpc;
import ru.microarch.delivery.adapters.grpc.java.Grpc;
import ru.microarch.delivery.domain.sharedkernel.Location;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

class GeoClientImplTest {

    @Test
    void testGetLocation() {
        // Setup
        GeoGrpc.GeoBlockingStub stub = mock(GeoGrpc.GeoBlockingStub.class);
        ManagedChannel channel = mock(ManagedChannel.class);
        when(stub.getChannel()).thenReturn(channel);

        Grpc.Location grpcLocation = Grpc.Location.newBuilder()
                .setX(5)
                .setY(3)
                .build();
        Grpc.GetGeolocationReply reply = Grpc.GetGeolocationReply.newBuilder()
                .setLocation(grpcLocation)
                .build();

        when(stub.getGeolocation(any(Grpc.GetGeolocationRequest.class))).thenReturn(reply);

        GeoClientImpl geoClient = new GeoClientImpl(stub);

        // Execute
        String testAddress = "123 Main St";
        Location location = geoClient.getLocation(testAddress);

        // Verify
        assertNotNull(location);
        assertEquals(5, location.x());
        assertEquals(3, location.y());

        // Verify interactions
        ArgumentCaptor<Grpc.GetGeolocationRequest> requestCaptor = ArgumentCaptor.forClass(Grpc.GetGeolocationRequest.class);
        verify(stub).getGeolocation(requestCaptor.capture());
        assertEquals(testAddress, requestCaptor.getValue().getAddress());

        verifyNoMoreInteractions(stub);
    }
}