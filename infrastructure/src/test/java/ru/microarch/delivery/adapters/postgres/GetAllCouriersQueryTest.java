package ru.microarch.delivery.adapters.postgres;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import ru.microarch.delivery.application.queries.getallcouriers.CourierDto;
import ru.microarch.delivery.application.queries.getallcouriers.GetAllCouriersHandler;
import ru.microarch.delivery.application.queries.getallcouriers.GetAllCouriersQuery;
import ru.microarch.delivery.application.queries.shareddto.LocationDto;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@Sql({"/20240423_01_courier_create.sql", "/02_courier_truncate.sql", "/03_courier_insert.sql"})
class GetAllCouriersQueryTest extends BaseDatabaseTest {

    @Autowired
    GetAllCouriersHandler handler;


    private static final CourierDto COURIER_1 = new CourierDto(
            UUID.fromString("123e4567-e89b-12d3-a456-426614174000"),
            "Turanga Leela",
            new LocationDto(3, 5)
    );

    private static final CourierDto COURIER_2 = new CourierDto(
            UUID.fromString("123e4567-e89b-12d3-a456-426614174001"),
            "Bender Bending Rodríguez",
            new LocationDto(7, 3)
    );

    @Test
    void testQuery() {
        List<CourierDto> allCouriers = handler.handle(new GetAllCouriersQuery());

        assertThat(allCouriers)
                .hasSize(2)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(COURIER_1, COURIER_2);
    }

}