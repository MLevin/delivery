package ru.microarch.delivery.adapters.kafka.order.status.changed;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.concurrent.CountDownLatch;

@Service
@Slf4j
@Getter
public class TestKafkaConsumerService {

    private String receivedMessage;
    private String receivedKey;
    private CountDownLatch latch;

    public void setLatch(CountDownLatch latch) {
        this.latch = latch;
    }

    @KafkaListener(topics = "${kafka.producer.topic}", groupId = "${spring.kafka.consumer.group-id}")
    public void listen(ConsumerRecord<String, String> record) {
        receivedMessage = record.value();
        receivedKey = record.key();
        if (latch != null) {
            latch.countDown();
        }
    }
}