package ru.microarch.delivery.adapters.kafka.order.status.changed;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.utility.DockerImageName;
import ru.microarch.delivery.adapters.kafka.order.status.changed.OrderStatusChangedIntegrationEvent.OrderStatus;
import ru.microarch.delivery.adapters.postgres.BaseDatabaseTest;
import ru.microarch.delivery.domain.order.aggregate.events.OrderCreatedDomainEvent;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;

import static org.junit.jupiter.api.Assertions.*;


class KafkaProducerTest extends BaseDatabaseTest {

    @Container
    public static KafkaContainer kafka = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:5.4.3"));

    @DynamicPropertySource
    static void kafkaProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.kafka.producer.bootstrap-servers", kafka::getBootstrapServers);
        registry.add("spring.kafka.consumer.bootstrap-servers", kafka::getBootstrapServers);
    }

    @Autowired
    private TestKafkaConsumerService consumerService;

    @Autowired
    private KafkaProducer kafkaProducer;
    private final ObjectMapper objectMapper = new ObjectMapper();

    private static final long TIMEOUT_MS = 5000; // 5 seconds timeout
    private static final long SLEEP_INTERVAL_MS = 100; // Sleep interval in milliseconds

    @Test
    void testSendingOrderStatusChangeEvent() throws Exception {
        final CountDownLatch latch = new CountDownLatch(1);
        consumerService.setLatch(latch);

        var event = new OrderCreatedDomainEvent(UUID.randomUUID());
        kafkaProducer.publishOrderCreatedDomainEvent(event);

        // assertTrue(latch.await(5, TimeUnit.SECONDS), "Timed out waiting for the Kafka message");
        // Can't use TimeUnit for some reason. Seems like JDK 21 problem
        boolean waitSuccess = awaitLatch(latch, TIMEOUT_MS);

        assertTrue(waitSuccess, "Timed out waiting for the Kafka message");

        verifyReceivedMessage(event);
    }

    private boolean awaitLatch(CountDownLatch latch, long timeoutMs) throws InterruptedException {
        final long startTime = System.currentTimeMillis();
        while (System.currentTimeMillis() - startTime < timeoutMs) {
            if (latch.getCount() == 0) {
                return true;
            }
            Thread.sleep(SLEEP_INTERVAL_MS); // Sleep for 100 milliseconds before checking again
        }
        return false;
    }

    private void verifyReceivedMessage(OrderCreatedDomainEvent expectedEvent) throws IOException {
        String receivedMessage = consumerService.getReceivedMessage();
        OrderStatusChangedIntegrationEvent receivedEvent = objectMapper.readValue(receivedMessage, OrderStatusChangedIntegrationEvent.class);
        String receivedKey = consumerService.getReceivedKey();

        assertAll("Verify correct message reception",
                () -> assertEquals(expectedEvent.getOrderId().toString(), receivedEvent.getOrderId(), "Order ID should match"),
                () -> assertEquals(OrderStatus.CREATED, receivedEvent.getOrderStatus(), "Order status should be CREATED"),
                () -> assertEquals(expectedEvent.getEventId().toString(), receivedKey, "Event key should match event ID")
        );
    }
}