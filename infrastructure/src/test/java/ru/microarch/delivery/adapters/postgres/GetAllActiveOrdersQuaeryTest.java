package ru.microarch.delivery.adapters.postgres;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import ru.microarch.delivery.application.queries.getactiveorders.GetActiveOrdersHandler;
import ru.microarch.delivery.application.queries.getactiveorders.GetActiveOrdersQuery;
import ru.microarch.delivery.application.queries.getactiveorders.OrderDto;
import ru.microarch.delivery.application.queries.shareddto.LocationDto;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@Sql({"/20240423_02_order_create.sql", "/01_order_truncate.sql", "/04_order_insert.sql"})
class GetAllActiveOrdersQuaeryTest extends BaseDatabaseTest {

    @Autowired
    GetActiveOrdersHandler querhandler;


    private static final OrderDto ORDER_1 = new OrderDto(
            UUID.fromString("123e4567-e89b-12d3-a456-426614174000"),
            new LocationDto(5, 5)
    );

    private static final OrderDto ORDER_2 = new OrderDto(
            UUID.fromString("123e4567-e89b-12d3-a456-426614174002"),
            new LocationDto(7, 7)
    );

    @Test
    void testQuery() {
        List<OrderDto> allCouriers = querhandler.handle(new GetActiveOrdersQuery());

        assertThat(allCouriers)
                .hasSize(2)
                .usingRecursiveFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(ORDER_1, ORDER_2);
    }

}