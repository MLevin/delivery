package ru.microarch.delivery.adapters.postgres;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import ru.microarch.delivery.domain.courier.aggregate.Courier;
import ru.microarch.delivery.domain.courier.aggregate.CourierStatus;
import ru.microarch.delivery.domain.courier.aggregate.Transport;
import ru.microarch.delivery.domain.sharedkernel.Location;
import ru.microarch.delivery.ports.CourierRepository;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Sql({"/20240423_01_courier_create.sql", "/02_courier_truncate.sql", "/03_courier_insert.sql"})
class CourierRepositoryTest extends BaseDatabaseTest {
    private static final UUID NOT_FOUND_ID = UUID.fromString("3d7b691e-8bb4-49b9-abc8-e0ccc1f8fd90");

    private static final Courier READY_COURIER = Courier.builder()
            .id(UUID.fromString("123e4567-e89b-12d3-a456-426614174000"))
            .name("Turanga Leela")
            .transport(Transport.fromId(2))
            .location(new Location(3, 5))
            .status(CourierStatus.READY)
            .build();

    private static final Courier BUSY_COURIER = Courier.builder()
            .id(UUID.fromString("123e4567-e89b-12d3-a456-426614174001"))
            .name("Bender Bending Rodríguez")
            .transport(Transport.fromId(3))
            .location(new Location(7, 3))
            .status(CourierStatus.BUSY)
            .build();

    private static final Courier NEW_COURIER = Courier.builder()
            .id(UUID.fromString("71839146-3ce0-47f1-8c77-4cd10df52cb2"))
            .name("Philip J. Fry")
            .transport(Transport.fromId(1))
            .location(new Location(4, 9))
            .status(CourierStatus.BUSY)
            .build();

    @Autowired
    private CourierRepository repository;


    @Test
    void whenGetByIdThenSuccess() {
        Courier courier = repository.get(READY_COURIER.getId());
        assertThat(courier).usingRecursiveComparison().isEqualTo(READY_COURIER);
    }

    @Test
    void whenIdNotFoundThenThrow() {
        var exception = assertThrows(NoSuchElementException.class, () -> repository.get(NOT_FOUND_ID));
        assertEquals("Not found entity with id=" + NOT_FOUND_ID, exception.getMessage());
    }

    @Test
    void whenAddThenReturnsAddedEntity() {
        Courier returnedCourier = repository.add(NEW_COURIER);
        assertThat(NEW_COURIER).usingRecursiveComparison().isEqualTo(returnedCourier);
    }

    @Test
    void whenAddThenDbGetsNewRecord() {
        repository.add(NEW_COURIER);
        Courier courierFromDb = repository.get(NEW_COURIER.getId());
        assertThat(NEW_COURIER).usingRecursiveComparison().isEqualTo(courierFromDb);
    }

    @Test
    void testGetAllReady() {
        List<Courier> allAssigned = repository.getAllReady();

        assertThat(allAssigned)
                .hasSize(1)
                .element(0).usingRecursiveComparison().isEqualTo(READY_COURIER);
    }

    @Test
    void testGetAllBusy() {
        List<Courier> allAssigned = repository.getAllBusy();

        assertEquals(1, allAssigned.size());
        assertThat(allAssigned.get(0)).usingRecursiveComparison().isEqualTo(BUSY_COURIER);
    }


    @Test
    void testUpdate() {
        var existingId = READY_COURIER.getId();

        Courier updatedCourier = Courier.builder()
                .id(existingId)
                .name("Dr. John A. Zoidberg")
                .transport(Transport.fromId(4))
                .location(new Location(1, 2))
                .status(CourierStatus.BUSY)
                .build();

        repository.update(updatedCourier);

        Courier courierFromDb = repository.get(updatedCourier.getId());
        assertThat(courierFromDb).usingRecursiveComparison().ignoringFields("version").isEqualTo(updatedCourier);
    }

    @Test
    void whenUpdatedCourierNotFoundThenThrow() {
        Courier updatedCourier = Courier.builder()
                .id(NOT_FOUND_ID)
                .name("Dr. John A. Zoidberg")
                .transport(Transport.CAR)
                .location(new Location(1, 2))
                .status(CourierStatus.BUSY)
                .build();

        var exception = assertThrows(NoSuchElementException.class, () -> repository.update(updatedCourier));
        assertEquals("Not found entity with id=" + NOT_FOUND_ID, exception.getMessage());
    }
}